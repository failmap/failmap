# Installation

## Development

If you just want to run websecmap for development, read [development/getting_started.html](../development/getting_started.html).

## Self-hosted

If you want to host websecmap please contact us at the email adres found here: https://internetcleanup.foundation/about-us/
