# Generated by Django 3.1.13 on 2022-09-19 11:31

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0010_auto_20220919_1000"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="scantype",
            name="plannable_activities",
        ),
        migrations.RemoveField(
            model_name="scantype",
            name="to",
        ),
        migrations.AddField(
            model_name="scanner",
            name="plannable_activities",
            field=models.ManyToManyField(to="scanners.ScannerActivity"),
        ),
        migrations.AddField(
            model_name="scanner",
            name="to",
            field=models.PositiveSmallIntegerField(choices=[(0, "endpoint"), (1, "url")], db_index=True, default=0),
        ),
        migrations.AlterField(
            model_name="scantype",
            name="category",
            field=models.ManyToManyField(blank=True, to="scanners.ScanTypeCategory"),
        ),
        migrations.AlterField(
            model_name="scantype",
            name="documentation_links",
            field=models.ManyToManyField(blank=True, to="scanners.ScanTypeDocumentationLink"),
        ),
        migrations.AlterField(
            model_name="scantype",
            name="second_opinion_links",
            field=models.ManyToManyField(blank=True, to="scanners.ScanTypeSecondOpinionLink"),
        ),
    ]
