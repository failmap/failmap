# Generated by Django 3.1.13 on 2022-09-07 08:13

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0006_internetnlv2scan_created_at_when"),
    ]

    operations = [
        migrations.AlterField(
            model_name="endpoint",
            name="port",
            field=models.IntegerField(db_index=True, default=443, help_text="Ports range from 1 to 65535."),
        ),
        migrations.AlterField(
            model_name="endpoint",
            name="protocol",
            field=models.CharField(
                db_index=True,
                help_text="Lowercase. Mostly application layer protocols, such as HTTP, FTP,SSH and so on. For more, read here: https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="endpointgenericscan",
            name="rating",
            field=models.CharField(
                db_index=True,
                default=0,
                help_text="Preferably an integer, 'True' or 'False'. Keep ratings over time consistent.",
                max_length=128,
            ),
        ),
        migrations.AlterField(
            model_name="endpointgenericscan",
            name="rating_determined_on",
            field=models.DateTimeField(
                db_index=True,
                help_text="This is when the current rating was first discovered. It may be obsoleted byanother rating or explanation (which might have the same rating). This date cannot change once it's set.",
            ),
        ),
        migrations.AlterField(
            model_name="urlgenericscan",
            name="rating",
            field=models.CharField(
                db_index=True,
                default=0,
                help_text="Preferably an integer, 'True' or 'False'. Keep ratings over time consistent.",
                max_length=128,
            ),
        ),
        migrations.AlterField(
            model_name="urlgenericscan",
            name="rating_determined_on",
            field=models.DateTimeField(
                db_index=True,
                help_text="This is when the current rating was first discovered. It may be obsoleted byanother rating or explanation (which might have the same rating). This date cannot change once it's set.",
            ),
        ),
    ]
