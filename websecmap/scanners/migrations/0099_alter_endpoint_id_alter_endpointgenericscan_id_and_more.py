# Generated by Django 4.2.13 on 2024-09-26 17:39

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0098_alter_domainip_is_the_latest_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="endpoint",
            name="id",
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name="endpointgenericscan",
            name="id",
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name="urlgenericscan",
            name="id",
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
