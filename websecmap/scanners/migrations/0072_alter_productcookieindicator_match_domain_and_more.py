# Generated by Django 4.2.3 on 2023-09-28 18:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0071_remove_productcookieindicator_match_field_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="productcookieindicator",
            name="match_domain",
            field=models.CharField(
                blank=True,
                help_text="Match cookie domain against this regex, is wrapped in '^$'.",
                max_length=200,
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="productcookieindicator",
            name="match_name",
            field=models.CharField(
                blank=True,
                help_text="Match cookie name against this regex, is wrapped in '^$'.",
                max_length=200,
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="productcookieindicator",
            name="match_path",
            field=models.CharField(
                blank=True,
                help_text="Match cookie path against this regex, is wrapped in '^$'.",
                max_length=200,
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="productcookieindicator",
            name="notes",
            field=models.TextField(
                blank=True,
                help_text="Provide context around how the relation with the cookie and product was found for future decisions to modify or remove this indicator.",
                null=True,
            ),
        ),
    ]
