# Generated by Django 3.1.13 on 2022-09-19 15:08

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("scanners", "0013_scanner_needs_results_from"),
    ]

    operations = [
        migrations.AlterField(
            model_name="scanner",
            name="needs_results_from",
            field=models.ForeignKey(
                blank=True, default="", null=True, on_delete=django.db.models.deletion.CASCADE, to="scanners.scanner"
            ),
        ),
    ]
