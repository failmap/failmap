# Generated by Django 4.2 on 2023-04-19 10:22

import taggit.managers
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("taggit", "0005_auto_20220424_2025"),
        ("scanners", "0044_endpoint_origin"),
    ]

    operations = [
        migrations.AddField(
            model_name="endpoint",
            name="security_policies",
            field=taggit.managers.TaggableManager(
                blank=True,
                help_text="A comma-separated list of tags.",
                through="taggit.TaggedItem",
                to="taggit.Tag",
                verbose_name="roles",
            ),
        ),
    ]
