"""Integration tests of scanner commands."""

import json

import pytest
from django.core.management import call_command

TEST_ORGANIZATION = "faalonië"
NON_EXISTING_ORGANIZATION = "faaloniet"


@pytest.mark.skip(reason="Test is unreliable and problem can't currently be fixed without multiple workers.")
def test_dummy(responses, db, faaloniae, default_scan_metadata):
    """Test running dummy scan."""

    # todo: dummy scan is not used right now. Not in db for lazyness
    result = json.loads(call_command("scan", "dummy", "-v3", "-o", TEST_ORGANIZATION))

    # dummy returns random success/failure results, only check if there is a result, not the result itself
    assert result[0] is None
