"""
The housekeeping scanner makes it possible to perform housekeeping on layers and urls. It can be planned and performed
just like any other scanner and will even produce an UrlGenericScan with the results of housekeeping tasks.

Housekeeping will reduce the amount of data in the database. This will speed it up as less records are needed.
"""

import json
import logging

from celery import Task, group

from websecmap.celery import app
from websecmap.scanners.scanmanager import store_url_scan_result
from websecmap.scanners.scanner.filter import (
    filter_urls_only,
)
from websecmap.scanners.housekeeping.tasks import perform_housekeeping

log = logging.getLogger(__name__)
SCANNER_NAME = "housekeeping"


@app.task(queue="kickoff", ignore_result=True)
def scan_all(**kwargs):
    for task in compose_scan_task(filter_urls_only(**kwargs)):
        task.apply_async(**kwargs.get("task_arguments", {}))
    return group()


def compose_manual_scan_task(
    organizations_filter: dict = None, urls_filter: dict = None, endpoints_filter: dict = None, **kwargs
):
    urls = filter_urls_only(organizations_filter, urls_filter, endpoints_filter, **kwargs)
    return group(compose_scan_task(urls))


def compose_scan_task(urls) -> list[Task]:
    return [scan.si(url.id) | store.s(url.pk) for url in urls]


@app.task(queue="storage", ignore_result=True)
def store(result: dict, url_id: int):
    store_url_scan_result("housekeeping", url_id, "completed", "", json.dumps(result))


@app.task(queue="storage")
def scan(url_id):
    return perform_housekeeping(url_id)
