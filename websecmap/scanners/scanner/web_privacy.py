"""
OpenWPM is a firefox browser that logs all responses to a database, from there it is possible to extract
what visits have been made, if they are secure and to what parties these requests are made.
https://openwpm.readthedocs.io/en/latest/Schema-Documentation.html

Thanks to:
- https://codeberg.org/dataskydd.net/
- https://github.com/PrivacyScore/privacyscanner/tree/whotracksme/privacyscanner/scanmodules
"""

import contextlib
import logging
import re
from collections import defaultdict
from json import JSONDecodeError
from typing import Any, Dict, List

import requests
from celery import group

import json

from websecmap.app.constance import constance_cached_value
from websecmap.celery import app
from websecmap.organizations.models import Url
from websecmap.scanners import init_dict, plannedscan
from websecmap.scanners.models import CookieIndicatorFields, Endpoint, EndpointGenericScan, ProductCookieIndicator
from websecmap.scanners.plannedscan import retrieve_endpoints_from_urls
from websecmap.scanners.scanmanager import store_endpoint_scan_result
from websecmap.scanners.scanner.filter import filter_endpoints_only, generic_plan_scan, merge_endpoints_filter
from websecmap.scanners.scanner.metric_constants import (
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_ERROR,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_HIGH,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_LOW,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_MEDIUM,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_OK,
    WEB_PRIVACY_COOKIES,
    WEB_PRIVACY_COOKIES_ERROR,
    WEB_PRIVACY_COOKIES_NEUTRAL,
    WEB_PRIVACY_THIRD_PARTY_DOMAINS,
    WEB_PRIVACY_THIRD_PARTY_DOMAINS_ERROR,
    WEB_PRIVACY_THIRD_PARTY_DOMAINS_NEUTRAL,
    WEB_PRIVACY_THIRD_PARTY_ERROR,
    WEB_PRIVACY_THIRD_PARTY_FOUND,
    WEB_PRIVACY_THIRD_PARTY_OK,
    WEB_PRIVACY_THIRD_PARTY_REQUESTS,
    WEB_PRIVACY_TRACKING,
    WEB_PRIVACY_TRACKING_ERROR,
    WEB_PRIVACY_TRACKING_OK,
    WEB_PRIVACY_TRACKING_TRACKERS_FOUND,
)
from websecmap.scanners.scanner.web_privacy_resources.trackers import get_known_trackers_from_urls
from websecmap.scanners.scanner_for_everything.location import (
    SCAN_TYPE_LOCATION_COOKIES,
    get_domain_to_ip_database,
    get_geoip_database,
    meaning_of_location_at_web_privacy_cookies,
)
from websecmap.scanners.housekeeping.reduce.uri import remove_randomization_from_url
from websecmap.utils import normalize_cookies

log = logging.getLogger(__name__)
SCANNER_NAME = "web_privacy"

# narrow down to the most restrictive set of domains, as the scanner is/can be pretty heavy on resource
# Site should be the same on v4 and v6 anyway... so don't bother with differences
STANDARD_ENDPOINT_FILTER = {
    "protocol": "https",
    "is_dead": False,
    "url__is_dead": False,
    "url__not_resolvable": False,
    "ip_version": 4,
    "port": 443,
}


@app.task(queue="kickoff", ignore_result=True)
def plan_scan(**kwargs) -> None:
    kwargs = merge_endpoints_filter(STANDARD_ENDPOINT_FILTER, **kwargs)
    generic_plan_scan(SCANNER_NAME, "scan", filter_endpoints_only, **kwargs)


@app.task(queue="kickoff")
def compose_planned_scan_task(**kwargs) -> group:
    urls = plannedscan.pickup(activity="scan", scanner=SCANNER_NAME, amount=kwargs.get("amount", 25))
    return compose_scan_task(urls)


def compose_manual_scan_task(
    organizations_filter: dict = None, urls_filter: dict = None, endpoints_filter: dict = None, **kwargs
) -> group:
    organizations_filter, urls_filter, endpoints_filter = init_dict(organizations_filter, urls_filter, endpoints_filter)
    # in this order the user can overwrite any part of the filter as this is last in the below merger
    # why was **kwargs['endpoint_filter'] used in merge_endpoint_filter, as it is also passed directly from the call.
    urls = filter_endpoints_only(organizations_filter, urls_filter, STANDARD_ENDPOINT_FILTER | endpoints_filter)
    log.debug("Scanning privacy on %s", urls)
    return compose_scan_task(urls)


def compose_scan_task(urls: List[Url]) -> group:
    https_endpoints, https_urls_without_endpoints = retrieve_endpoints_from_urls(
        urls, protocols=["https"], ports=[443], ip_versions=[4]
    )

    tasks = [plannedscan.finish.si("scan", SCANNER_NAME, url_id) for url_id in https_urls_without_endpoints]
    tasks.extend(
        group(
            scan.si(endpoint.uri_url())
            | store.s(endpoint.id)
            | plannedscan.finish.si("scan", SCANNER_NAME, endpoint.url.id)
        )
        for endpoint in https_endpoints
    )
    return group(tasks)


@app.task(queue="playwright-privacy", ignore_result=True)
def scan(url: str) -> Dict[str, Any]:
    # just except the loudest way possible in case of exceptions.
    log.debug("Scanning privacy on %s", url)
    # this is a single threaded webserver. More tasks means more waiting. So keep the amount of tasks low enough
    # so the timeout is never reached in case all tasks turn into timeouts
    response = requests.get(f"{constance_cached_value('WEB_PRIVACY_SERVER')}?site={url}", timeout=300)
    # split the call to json so in case of an exception the result from the server gets logged in the stack trace
    # don't recover from this error as this needs to work right all the time.
    json_response = response.json()
    log.debug("Privacy metrics returned %s", json_response)
    return json_response


@app.task(queue="storage", ignore_result=True)
def store(result_data: Dict[str, Any], endpoint_id: int) -> None:
    """
    {
      "third-party-requests": [
        "https://ab.tweakers.nl/pool/lib/3751_0.gif",
        "https://aa.tweakers.nl/json",
        ...
      ],
      "third-party-requests-structured": [
        {"url": "https://a.tile.osm.org/7/62/64.png", "resource_type": "image", "method": "GET", "post_data": None},
        {"url": "https://a.tile.osm.org/7/63/63.png", "resource_type": "image", "method": "GET", "post_data": None},
      ],
      "third-party-domains": [
        "ab.tweakers.nl",
        "aa.tweakers.nl"
      ],
      "third-party-top-level-domains": [
        "tweakers.nl"
      ],
      "cookies": [
        {
          "name": "__Secure-TnetID",
          "value": ".0ZJcr8PVkq_xDqzVBaqpqFrJG4Jhikgj",
          "domain": ".tweakers.net",
          "path": "/",
          "expires": 1683477449.479243,
          "httpOnly": false,
          "secure": true,
          "sameSite": "Lax"
        },
        ...
      ],
      "timeless-cookies": [
        {
          "name": "__Secure-TnetID",
          "domain": ".tweakers.net",
          "path": "/",
          "httpOnly": false,
          "secure": true,
          "sameSite": "Lax"
        },
        ...
      ],
      "headers": {
        "cache-control": "max-age=0, private",
        "content-encoding": "gzip",
        "content-security-policy": "default-src https: data: blob: 'unsafe-inline' 'unsafe-eval'; ...
        "content-type": "text/html; charset=UTF-8",
        "date": "Wed, 03 May 2023 16:37:29 GMT",
        "expires": "-1",
        "server": "Apache",
        "strict-transport-security": "max-age=31536000; includeSubDomains; preload",
        "vary": "Accept-Encoding",
        "x-clacks-overhead": "GNU Terry Pratchett, Yoeri Lauwers, Arie Jan Stapel",
        "x-content-type-options": "nosniff",
        "x-tweakers-server": "twk-eun-web1",
        "x-xss-protection": "1; mode=block"
      },
      "security_details": {
        "issuer": "R3",
        "protocol": "TLS 1.3",
        "subjectName": "*.tweakers.net",
        "validFrom": 1682935528,
        "validTo": 1690711527
      },
      "status": 200
    }

    """

    endpoint = Endpoint.objects.all().filter(id=endpoint_id).first()
    if not endpoint:
        return

    if error := result_data.get("error"):
        # An error means that previous metrics have no value anymore, so store the new erroneous state for
        # all of the sub-scans.

        store_endpoint_scan_result(
            WEB_PRIVACY_TRACKING, endpoint_id, WEB_PRIVACY_TRACKING_ERROR, WEB_PRIVACY_TRACKING_ERROR, evidence=error
        )
        store_endpoint_scan_result(
            WEB_PRIVACY_THIRD_PARTY_REQUESTS,
            endpoint_id,
            WEB_PRIVACY_THIRD_PARTY_ERROR,
            WEB_PRIVACY_THIRD_PARTY_ERROR,
            evidence=error,
        )
        store_endpoint_scan_result(
            WEB_PRIVACY_COOKIES, endpoint_id, WEB_PRIVACY_COOKIES_ERROR, WEB_PRIVACY_COOKIES_ERROR, evidence=error
        )

        store_endpoint_scan_result(
            WEB_PRIVACY_THIRD_PARTY_DOMAINS,
            endpoint_id,
            WEB_PRIVACY_THIRD_PARTY_DOMAINS_ERROR,
            WEB_PRIVACY_THIRD_PARTY_DOMAINS_ERROR,
            evidence=error,
        )

        store_endpoint_scan_result(
            SCAN_TYPE_LOCATION_COOKIES,
            endpoint_id,
            "scan_error",
            "scan_error",
            evidence=error,
        )

        store_endpoint_scan_result(
            WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT,
            endpoint_id,
            WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_ERROR,
            WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_ERROR,
            evidence=error,
        )

        return

    third_party_requests = result_data.get("third-party-requests-structured", [])
    # clean up third party requests, this removes a ton of records in the database.
    third_party_requests = remove_random_data_from_third_party_requests(third_party_requests)
    # it's easy to have more than 9000 characters with third party stuff. there make this shorter
    # This removes urls with the same parameters but different content. As that's just more of the same.
    third_party_requests = remove_duplicate_third_party_requests(third_party_requests)
    # limit the total amount of requests to 500, which is already more than insanely ridiculous.
    # That would mean several fonts, tons of images, external css and whatnot. This prevents that the
    # evidence grows to gigabytes.
    third_party_requests = third_party_requests[:500]
    # don't store extremely long post data, which has little value. 200 chars is already insane for a site visit.
    third_party_requests = truncate_long_post_data(third_party_requests, 200)
    third_party_domains = result_data.get("third-party-domains", [])

    known_trackers = get_known_trackers_from_urls(third_party_domains)

    # happens sometimes, where it should not
    web_privacy_tracking_rating = WEB_PRIVACY_TRACKING_TRACKERS_FOUND if known_trackers else WEB_PRIVACY_TRACKING_OK
    store_endpoint_scan_result(
        scan_type=WEB_PRIVACY_TRACKING,
        endpoint_id=endpoint_id,
        rating=web_privacy_tracking_rating,
        message=web_privacy_tracking_rating,
        evidence=known_trackers,
    )

    # happens a lot, due to easiest path of development / culture
    # todo: to avoid duplicate findings ignore content of parameters, only check for parameters and addresses
    #  see comparators.py
    web_third_party_rating = WEB_PRIVACY_THIRD_PARTY_FOUND if third_party_requests else WEB_PRIVACY_THIRD_PARTY_OK
    store_endpoint_scan_result(
        scan_type=WEB_PRIVACY_THIRD_PARTY_REQUESTS,
        endpoint_id=endpoint_id,
        rating=web_third_party_rating,
        message=web_third_party_rating,
        evidence=json.dumps(third_party_requests),
    )

    # timeless-cookies are cookies with unique components (like: value, timestamp) stripped
    cookies = normalize_cookies(result_data.get("timeless-cookies", []))
    products = product_meaning_from_cookies(cookies)
    cookies_product_meaning = {
        "products": products["products"],
    }

    store_endpoint_scan_result(
        scan_type=WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT,
        endpoint_id=endpoint_id,
        rating=products["worst_purpose_rating"],
        message=WEB_PRIVACY_COOKIES_NEUTRAL,
        meaning=cookies_product_meaning,
        evidence=json.dumps(cookies),
    )

    locations_worst_rating, locations_rating_series = meaning_of_location_at_web_privacy_cookies(cookies)
    cookies_location_meaning = {
        "locations_worst_rating": locations_worst_rating,
        "locations": locations_rating_series,
    }

    store_endpoint_scan_result(
        scan_type=SCAN_TYPE_LOCATION_COOKIES,
        endpoint_id=endpoint_id,
        rating=locations_worst_rating,
        message=locations_worst_rating,
        meaning=cookies_location_meaning,
        evidence=json.dumps(cookies),
    )

    store_endpoint_scan_result(
        scan_type=WEB_PRIVACY_COOKIES,
        endpoint_id=endpoint_id,
        rating=WEB_PRIVACY_COOKIES_NEUTRAL,
        message=WEB_PRIVACY_COOKIES_NEUTRAL,
        meaning={**cookies_product_meaning, **cookies_location_meaning},
        evidence=json.dumps(cookies),
    )

    store_endpoint_scan_result(
        scan_type=WEB_PRIVACY_THIRD_PARTY_DOMAINS,
        endpoint_id=endpoint_id,
        rating=WEB_PRIVACY_THIRD_PARTY_DOMAINS_NEUTRAL,
        message=WEB_PRIVACY_THIRD_PARTY_DOMAINS_NEUTRAL,
        evidence=json.dumps(third_party_domains),
    )


def rate_purpose_on_cookie_set_without_consent(purpose):
    if purpose in constance_cached_value("COOKIE_WHEN_RECEIVED_WITHOUT_CONSENT_PURPOSES_HIGH"):
        return WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_HIGH
    if purpose in constance_cached_value("COOKIE_WHEN_RECEIVED_WITHOUT_CONSENT_PURPOSES_MEDIUM"):
        return WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_MEDIUM
    if purpose in constance_cached_value("COOKIE_WHEN_RECEIVED_WITHOUT_CONSENT_PURPOSES_LOW"):
        return WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_LOW
    return WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_OK


# todo: convert to ordered enum
COOKIE_NO_CONSENT_RATING_ORDER = [
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_HIGH,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_MEDIUM,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_LOW,
    WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_OK,
]


def worse_rating(rating_a, rating_b):
    if COOKIE_NO_CONSENT_RATING_ORDER.index(rating_a) <= COOKIE_NO_CONSENT_RATING_ORDER.index(rating_b):
        return rating_a
    return rating_b


def product_meaning_from_cookies(cookies=None) -> dict[str:Any]:
    """Generate list of products determined from cookies."""
    if cookies is None:
        cookies = []
    cookie_indicators = []

    worst_rating = WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT_OK

    # to get the right and purpose for each cookie, it's better to have each cookieproductindicator
    # listed separately per cookie.
    for indicator in ProductCookieIndicator.objects.all().prefetch_related(
        "product", "product__vendor", "most_significant_purpose"
    ):
        matchers = {}
        if indicator.match_name:
            matchers["name"] = re.compile(f"^{indicator.match_name}$")
        if indicator.match_domain:
            matchers["domain"] = re.compile(f"^{indicator.match_domain}$")
        if indicator.match_path:
            matchers["path"] = re.compile(f"^{indicator.match_path}$")
        cookie_indicators.append((matchers, indicator))

    products = defaultdict(lambda: dict([["cookies", list()]]))  # pylint: disable=use-list-literal
    for cookie in cookies:
        for matchers, indicator in cookie_indicators:
            if all(matcher.match(cookie[field]) for field, matcher in matchers.items()):
                # flat lists with direct keys versus complexity in nesting :)
                products[indicator.product.id]["product_id"] = indicator.product.id
                products[indicator.product.id]["product_name"] = indicator.product.name
                products[indicator.product.id]["product_version"] = "unknown"
                if indicator.product.vendor:
                    products[indicator.product.id]["product_vendor_name"] = indicator.product.vendor.name
                    products[indicator.product.id]["product_vendor_id"] = indicator.product.vendor.id
                else:
                    products[indicator.product.id]["product_vendor_name"] = None
                    products[indicator.product.id]["product_vendor_id"] = None

                # only select the fields from the cookie that where relevant for this selection
                my_cookie = strip_irrelevant_cookie_fields(cookie)

                # and add information about the purpose of the cookie and what it means
                # The indicator id is where all the other relevant info from the cookie
                # database comes from in the UI. That is not used to further judge a cookie but just
                # for information purposes. Which saves a lot of references. This might also
                # become a table, but why.
                my_cookie["indicator"] = indicator.id
                my_cookie["purpose"] = (
                    indicator.most_significant_purpose.name if indicator.most_significant_purpose else ""
                )
                rating = rate_purpose_on_cookie_set_without_consent(my_cookie["purpose"])
                my_cookie["purpose_rating"] = rating
                worst_rating = worse_rating(worst_rating, rating)

                products[indicator.product.id]["cookies"].append(my_cookie)

                break
        else:
            log.debug("No product match for cookie: %s", cookie)

    # todo: get the worst rating for this cookie and add that as well.
    return {"products": list(products.values()), "worst_purpose_rating": worst_rating}


def strip_irrelevant_cookie_fields(cookie: dict, keep_fields={x[1] for x in CookieIndicatorFields.choices}) -> dict:
    """Strip fields from cookies that where not used to determine product."""
    return {k: cookie[k] for k in cookie if k in keep_fields}


def privacy_counters(add_urls: bool = False) -> None:
    scans = EndpointGenericScan.objects.all().filter(
        type="web_privacy_tracking",
        is_the_latest_scan=True,
        endpoint__is_dead=False,
        endpoint__url__is_dead=False,
        endpoint__url__not_resolvable=False,
    )
    # todo: specify layers
    keys = ["Company", "Platform", "Tracking Url"]
    for key in keys:
        companies = {}
        for my_scan in scans:
            with contextlib.suppress(JSONDecodeError):
                evidence = json.loads(my_scan.evidence.replace("'", '"'))
                for tracker in evidence:
                    if tracker[key] not in companies:
                        companies[tracker[key]] = {"amount": 0, "urls": []}
                    companies[tracker[key]]["amount"] += 1
                    if add_urls:
                        companies[tracker[key]]["urls"].append(my_scan.endpoint.url.url)
        print(key)
        print(json.dumps(companies, indent=4))


def truncate_long_post_data(third_party_requests: List[Dict[str, str]], max_chars: int = 60) -> List[Dict[str, str]]:
    """
    60 characters is long enough to see what about is being sent, but to too long.
      {
        "url": "https://cuatro.sim-cdn.nl/assets/1.93.0/iconFonts/fontawesome6/css/fontawesome.6.4.0.css",
        "resource_type": "stylesheet",
        "method": "GET",
        "post_data": null
      },
    """

    truncation_message_length = 35
    for request in third_party_requests:
        if request["post_data"] and len(request["post_data"]) > max_chars + truncation_message_length:
            truncated = len(request["post_data"][max_chars:])
            request["post_data"] = request["post_data"][:max_chars] + "... truncated " + str(truncated) + " characters"

    return third_party_requests


def remove_random_data_from_third_party_requests(third_party_requests: List[Dict[str, str]]) -> List[Dict[str, str]]:
    for request in third_party_requests:
        request["url"] = remove_randomization_from_url(request["url"])
    return third_party_requests


def remove_random_data_from_existing_third_party_requests() -> None:
    # This is the third approach to cleaning up this table.
    # A: Using limit/offset does not work. (so updating batches of 10k) it did work, but was very slow causing timeouts
    # B: Using a select id, evidence query does not work as that also caused timeouts
    # C: Using ID and then create a new task that just updates that record will probably work.
    #  - Direct access to an ID is instant
    #  - The query to get all 4 million id's takes 2.47 seconds
    # just run this from a shell somewhere....
    scans = EndpointGenericScan.objects.all().filter(type=WEB_PRIVACY_THIRD_PARTY_REQUESTS).only("id")
    for my_scan in scans:
        print(f"Processing scan {my_scan.id}")
        remove_random_data_from_existing_third_party_request(my_scan.id)


@app.task(queue="database", ignore_result=True)
def remove_random_data_from_existing_third_party_request(scan_id: int):
    my_scan = EndpointGenericScan.objects.filter(pk=scan_id).only("id", "evidence", "type").first()
    if not my_scan:
        log.error("Could not find endpointgenericscan with id %d", scan_id)
        return
    # not added in the query as that will make the query slower :)
    if my_scan.type != WEB_PRIVACY_THIRD_PARTY_REQUESTS:
        log.error("Endpointgenericscan has the wrong scan type, with id %d", scan_id)
        return
    if not my_scan.evidence:
        return
    try:
        third_party_requests = json.loads(my_scan.evidence)
    except JSONDecodeError:
        return
    third_party_requests = remove_random_data_from_third_party_requests(third_party_requests)
    third_party_requests = remove_duplicate_third_party_requests(third_party_requests)
    my_scan.evidence = json.dumps(third_party_requests)
    my_scan.save(update_fields=["evidence"])


def remove_duplicate_third_party_requests(third_party_requests: List[Dict[str, str]]) -> List[Dict[str, str]]:
    # This used to be complex, but as urls now have their randomization reduced and their values removed we don't
    # need to parse the query strings and so on anymore. We can just use a classic dictionary comparison like
    # https://stackoverflow.com/questions/9427163/remove-duplicate-dict-in-list-in-python
    return sorted([dict(t) for t in {tuple(d.items()) for d in third_party_requests}], key=lambda x: x["url"])


@app.task(queue="database", ignore_result=True)
def update_meaning_of_location_and_products_at_web_privacy_cookies():
    """Update existing endpointgenericscan objects to add location or product meaning."""
    geo_ip_data = get_geoip_database()
    domain_ip_data = get_domain_to_ip_database()

    endpointgenericscans = EndpointGenericScan.objects.all().filter(type=WEB_PRIVACY_COOKIES, is_the_latest_scan=True)
    log.debug("Found %d endpoints with site content", endpointgenericscans.count())

    for endpointgenericscan in endpointgenericscans:
        add_meaning_of_location_and_products_at_web_privacy_cookies(endpointgenericscan, domain_ip_data, geo_ip_data)


def add_meaning_of_location_and_products_at_web_privacy_cookies(
    endpointgenericscan: EndpointGenericScan, domain_ip_data=None, geo_ip_data=None
):
    endpoint_id = endpointgenericscan.endpoint.id

    # in the past evidence was truncated leading to invalid JSON
    try:
        cookies = json.loads(endpointgenericscan.evidence)
    except JSONDecodeError:
        log.debug("failed to get cookies from evidence")
        cookies = []

    products = product_meaning_from_cookies(cookies)
    cookies_product_meaning = {
        "products": products["products"],
    }

    store_endpoint_scan_result(
        scan_type=WEB_PRIVACY_COOKIE_PRODUCTS_NO_CONSENT,
        endpoint_id=endpoint_id,
        rating=products["worst_purpose_rating"],
        message=WEB_PRIVACY_COOKIES_NEUTRAL,
        meaning=cookies_product_meaning,
        evidence=json.dumps(cookies),
    )

    locations_worst_rating, locations_rating_series = meaning_of_location_at_web_privacy_cookies(cookies)
    cookies_location_meaning = {
        "locations_worst_rating": locations_worst_rating,
        "locations": locations_rating_series,
    }

    store_endpoint_scan_result(
        scan_type=SCAN_TYPE_LOCATION_COOKIES,
        endpoint_id=endpoint_id,
        rating=locations_worst_rating,
        message=locations_worst_rating,
        meaning=cookies_location_meaning,
        evidence=json.dumps(cookies),
    )

    store_endpoint_scan_result(
        scan_type=WEB_PRIVACY_COOKIES,
        endpoint_id=endpoint_id,
        rating=WEB_PRIVACY_COOKIES_NEUTRAL,
        message=WEB_PRIVACY_COOKIES_NEUTRAL,
        meaning={**cookies_product_meaning, **cookies_location_meaning},
        evidence=json.dumps(cookies),
    )
