"""
Check if the https site uses HSTS to tell the browser the site should only be reached over HTTPS.
(useful until browsers do https by default, instead of by choice)

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
prevents reflected xss

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
prevents clickjacking

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
forces the content-type to be leading for defining a type of file (not the browser guess)
The browser guess could execute the file, for example with the wrong plugin.
Basically the server admin should fix the browser, instead of the other way around.

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
Will be the browser default. Forces https, even if http resources are available.

The preload list is idiotic: it should contain any site in the world.
A whopping 1 municipality in NL uses the preload list (eg knows if it's existence).
preload list is obscure and a dirty fix for a structural problem.

Another weird thing is: the default / recommendation for hsts is off. Many sites, esp. in
governments have a once-a-year cycle for doing something requires. So HSTS should be
longer than a year, like one year and three months. Some site punish long hsts times.

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Public-Key-Pins
Has the potential to make your site unreachable if not properly (automatically) maintained
The backup cert strategy is also incredibly complex. Creating the right hash is also hard.
So if you don't use this. There should be another way to link the content of the site to
the transport.
header likely to be killed like p3p
'Public-Key-Pins': 0,
Classified as: Ignored

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
Very complex header to specify what resources can be loaded from where. Especially useful
when loading in third party content such as horrible ads. Prevents against xss

Flash, PDF and other exploit prone things can be embedded. Should never happen:
the content should always be none(?).
if not set to none, it is 200 points for allowing flash and pdf to be embedded at all :)
'X-Permitted-Cross-Domain-Policies':
Classified as: Ignored

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
largely unsupported
What referrer should be allowed to access the resource. Security on referrer headers? No.
'Referrer-Policy':
Classified as: Ignored
"""

import contextlib
import copy
import json
import logging
import re
from typing import Any, Dict, List, Union
from urllib.parse import urlparse, urlsplit, urlunsplit

import niquests
from celery import Task, group
from niquests.structures import CaseInsensitiveDict
from tldextract import tldextract

from websecmap.celery import app
from websecmap.celery.worker import WORKER_ROLE
from websecmap.organizations.models import Organization, OrganizationType, Url
from websecmap.scanners import init_dict
from websecmap.scanners.models import Endpoint, EndpointGenericScan
from websecmap.scanners.scanmanager import store_endpoint_scan_result
from websecmap.scanners.scanner.__init__ import allowed_to, q_configurations_to_scan, unique_and_random
from websecmap.scanners.scanner.http import create_standard_request_headers, get_random_user_agent
from websecmap.scanners.scanner.utils import IP_QUEUES
from websecmap.scanners.security_policies import add_policy_by_name

log = logging.getLogger(__name__)

DISABLE_IPV6 = WORKER_ROLE == "v4_internet"
DISABLE_IPV4 = WORKER_ROLE == "v6_internet"

HEADER_RETRIEVAL_TIMEOUT = 20
# total timeout for task, 2x the normal timeout to account for redirects, dns/connect timeouts
HEADER_RETRIEVAL_TRANSFER_TIMEOUT = HEADER_RETRIEVAL_TIMEOUT * 2.5

SECURITY_HEADER_SCAN_TYPES = [
    "http_security_header_strict_transport_security",
    "http_security_header_x_content_type_options",
    "http_security_header_x_frame_options",
    "http_security_header_x_xss_protection",
    "http_security_header_content_security_policy",
    "http_security_header_permissions_policy",
    "http_security_header_referrer_policy",
]


def filter_scan(
    organizations_filter: dict = None, urls_filter: dict = None, endpoints_filter: dict = None, **kwargs
) -> List[Endpoint]:
    organizations_filter, urls_filter, endpoints_filter = init_dict(organizations_filter, urls_filter, endpoints_filter)

    # apply filter to organizations (or if no filter, all organizations)
    # apply filter to urls in organizations (or if no filter, all urls)
    if organizations_filter:
        organizations = Organization.objects.filter(**organizations_filter).only("id")
        urls = Url.objects.filter(q_configurations_to_scan(), organization__in=organizations, **urls_filter)
    else:
        urls = Url.objects.filter(q_configurations_to_scan(), **urls_filter)

    # Don't scan where you will not connect or where there is an administrative no (dead) on the url.
    urls = urls.filter(is_dead=False, not_resolvable=False)

    # We only perform an IN query, and need nothing of these urls except the ID:
    urls = urls.only("id")

    # select endpoints to scan based on filters.
    # use the right protocols, don't visit http://example.nl:443 for example, even though there might be
    # a site there with a 'you issued a http request to an https site'.
    https_endpoints = Endpoint.objects.filter(
        # apply filter to endpoints (or if no filter, all endpoints)
        url__in=urls,
        **endpoints_filter,
        # also apply mandatory filters to only select valid endpoints for this action
        is_dead=False,
        protocol="https",
        port__in=[443, 8443],
    ).only("id", "port", "protocol", "ip_version", "url__id", "url__url")

    http_endpoints = Endpoint.objects.filter(
        # apply filter to endpoints (or if no filter, all endpoints)
        url__in=urls,
        **endpoints_filter,
        # also apply mandatory filters to only select valid endpoints for this action
        is_dead=False,
        protocol="http",
        # officially http, would guess it is https most of the time anyway. Let's determine it instead assume.
        port__in=[80, 8080, 8008],
    ).only("id", "port", "protocol", "ip_version", "url__id", "url__url")

    return unique_and_random(list(https_endpoints) + list(http_endpoints))


@app.task(queue="kickoff", ignore_result=True)
def scan_all(organizations_filter: dict = None, urls_filter: dict = None, endpoints_filter: dict = None, **kwargs):
    organizations_filter, urls_filter, endpoints_filter = init_dict(organizations_filter, urls_filter, endpoints_filter)
    if not allowed_to("scan", "security_headers"):
        return group()

    endpoints = filter_scan(organizations_filter, urls_filter, endpoints_filter, **kwargs)
    for task in compose_scan_tasks(endpoints):
        task.apply_async(**kwargs.get("task_arguments", {}))

    # return a group for legacy reasons, other stuff expects this so it can apply things itself.
    return group()


def compose_manual_scan_task(
    organizations_filter: dict = None, urls_filter: dict = None, endpoints_filter: dict = None, **kwargs
) -> Task:
    organizations_filter, urls_filter, endpoints_filter = init_dict(organizations_filter, urls_filter, endpoints_filter)

    """Compose taskset to scan specified endpoints."""
    if not allowed_to("scan", "security_headers"):
        return group()

    # this gets as large as the user requests it to be. Usually one url.
    endpoints = filter_scan(organizations_filter, urls_filter, endpoints_filter, **kwargs)
    return group(compose_scan_tasks(endpoints))


def compose_scan_tasks(endpoints: List[Endpoint]) -> List[Task]:
    # user agent should be determined at queue time as it is a DB call that may not run on internet workers
    user_agent = get_random_user_agent()
    return [
        (
            get_headers.si(endpoint.uri_url(), endpoint.host(), user_agent).set(queue=IP_QUEUES[endpoint.ip_version])
            | analyze_headers.s(endpoint.pk)
        )
        for endpoint in endpoints
    ]


def discover_service_type(headers: CaseInsensitiveDict = None):
    """
    Try to discover some meaning of the webserver headers, because some HTTP servers are used for entirely different
    purposes than hosting web pages. For example SOAP is used for XML messages and has their own security paradigm.

    :param headers:
    :return:
    """
    if not headers:
        log.debug("No headers present, falling back to service type HTTP.")
        return "http"

    # Soap messages, they don't need any standard http web headers at all.
    # known are: X-WSSecurity-Enabled, X-WSSecurity-For, X-OAuth-Enabled, WWW-Authenticate
    # We trust that this header is not just set to circumvent HTTP security checks in the meanwhile.
    # Soap security checks are harder, and headers are much harder to implement correctly.
    if "X-SOAP-Enabled" in headers:
        log.debug("Service type to be discovered as: SOAP.")
        return "soap"

    # Do not assume headers for sites with Basic Authentication and login portals. The application behind the
    # login can set the headers to whatever it sees fit. Connecting those sites to the internet is still stupid,
    # but there's probably a reason for it.
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/WWW-Authenticate
    # https://www.iana.org/assignments/http-authschemes/http-authschemes.xhtml
    # In the future we _CAN_ however list insecure authentication methods as such, meaning getting rid of
    # basic authentication.
    if "www-authenticate" in headers:
        log.debug("Authentication required to discover headers. Reporting this as unknown.")
        return "access_restricted"

    # We're going to ignore all service types, except whatever looks like html/xhtml/etc
    # So text/plain, text/json and such are ignored.
    content_type = headers.get("Content-Type", "unknown")
    html_content_types = [
        "text/html; charset=utf-8",
        "text/html",
        "application/xhtml+xml",
        "application/xhtml",
        "html",
        "htm",
        "xhtml",
        "application/xhtml + xml",
        "text/html;charset=utf-8",
    ]

    # text/html can have many charsets, such as us-ascii.
    if content_type.lower() in html_content_types or "text/html" in content_type.lower():
        return "http"

    if content_type.lower() == "application/json":
        return "json"

    if content_type.lower() == "application/xml":
        return "xml"

    # text is a typical thing to protect with headers i guess
    # if content_type.lower() == "text/plain":
    #     return "text"

    log.debug("Unknown service type discovered.")
    return "non_http_response"


@app.task(queue="database")
def analyze_headers(headers: Union[bool, List[Dict[str, Any]]], endpoint_id):
    # todo: remove code paths, and make a more clear case per header type. That's easier to understand edge cases.
    # todo: Content-Security-Policy, Referrer-Policy

    # if scan task failed, ignore the result (exception) and report failed status
    # Exceptions do not serialize.
    if isinstance(headers, (bool, Exception)):
        """
        There could be many reason the parent failed. Some issues however are created by mandated certificate use.
        In this case a "ConnectionError", "('Connection aborted.', OSError(\"(104, 'ECONNRESET')\",)) is received.
        An active connection reset indicates that you'll not be ever able to update scan results.

        This is a nice edge case, since the endpoint still exist and the certificate is also valid. But the
        headers cannot ever be retrieved. This causes header information to be outdated for months or years.

        For this case another state had been defined: unreachable. This is a header state that is seen as good. It
        will be applied to all existing headers of this endpoint, in order to clean up what is already there.

        """

        existing_header_scans = EndpointGenericScan.objects.all().filter(
            endpoint=endpoint_id, type__in=SECURITY_HEADER_SCAN_TYPES, is_the_latest_scan=True
        )

        # Do not store 'corrections' when there are no scans already.
        if not existing_header_scans:
            # kombu.exceptions.EncodeError: Object of type ParentFailed is not JSON serializable
            # return ParentFailed("Skipping http header result parsing because scan failed.", cause=headers)
            return {"status": "unreachable-without-prior-scans"}

        # There used to be stringent filtering here: for oserror and ECONNRESET in the exception, but the fact is
        # that there are so many possible network errors, that it's always a struggle to keep up to date.
        # Instead of handling every edge case, make sure that the existing headers are set to unreachable,
        # and that the evidence shows what went wrong for later debugging reasons.

        for scan in existing_header_scans:
            store_endpoint_scan_result(
                scan.type, endpoint_id, "unreachable", "Address became unreachable.", str(headers)
            )

        return {"status": "unreachable-set-to-unreachable"}

    # prevent all issues with capitalization, x-XSS and X-Xss etc are all used out there
    last_redirect_headers = headers[-1]["headers"]

    # Discover the service type of the last set of headers after possible redirects. Based on this service type
    # not all headers are mandatory. This includes the HSTS header which is mandated by law, even though
    # automated programs will not care about the HSTS header.
    service_type = discover_service_type(CaseInsensitiveDict(last_redirect_headers))

    endpoint = Endpoint.objects.all().filter(pk=endpoint_id).only("pk", "url__id", "protocol").first()
    if not endpoint:
        return {"status": "success"}

    if service_type == "http":
        # normal webserver role
        return analyze_website_headers(endpoint_id, endpoint.url.id, endpoint.protocol, headers)
    if service_type in ["soap", "xml", "json"]:
        add_policy_by_name(endpoint_id, f"{service_type}_api")
        return analyze_soap_headers(endpoint_id, service_type)
    if service_type == "non_http_response":
        return clean_up_existing_headers(endpoint_id, service_type=service_type, reason="unknown_content_type")
    if service_type == "access_restricted":
        return clean_up_existing_headers(endpoint_id, service_type=service_type, reason="authentication_required")

    # fallback
    return clean_up_existing_headers(endpoint_id, service_type=service_type, reason="unknown_content_type")


def analyze_soap_headers(endpoint_id, service_type: str = "soap"):
    """
    We currently have no implementation for SOAP headers, but we do know that previously discovered non-soap headers
    can be overwritten as being SOAP headers and not being relevant anymore.

    There is no implementation for checking the security of soap headers, although a soap service should be using
    some of the following: X-WSSecurity-Enabled, X-WSSecurity-For, X-OAuth-Enabled, WWW-Authenticate

    A next iteration of websecmap could/should contain this validation that certain headers are mandated for SOAP.

    :param endpoint_id:
    :return:
    """

    # clean up existing web headers and set them to being not relevant for soap:
    existing_header_scans = EndpointGenericScan.objects.all().filter(
        endpoint=endpoint_id, type__in=SECURITY_HEADER_SCAN_TYPES, is_the_latest_scan=True
    )
    for scan in existing_header_scans:
        store_endpoint_scan_result(scan.type, endpoint_id, service_type, f"header_not_relevant_for_{service_type}")

    return {"status": "success"}


def clean_up_existing_headers(endpoint_id: int, service_type: str, reason: str):
    """
    Unknown headers for a content type we can't handle.

    We do NOT create new headers, meaning that if no relevant data was found, no records are added to the database.

    :param endpoint_id:
    :param service_type: What type of service has been discovered that prevents further processing: RESTRICTED, UNKNOWN
    :param reason: More verbose explanation of the service type.
    :return:
    """

    # clean up existing web headers and set them to being not relevant for soap:
    existing_header_scans = EndpointGenericScan.objects.all().filter(
        endpoint=endpoint_id, type__in=SECURITY_HEADER_SCAN_TYPES, is_the_latest_scan=True
    )

    for scan in existing_header_scans:
        store_endpoint_scan_result(scan.type, endpoint_id, service_type, reason)

    return {"status": "success"}


def extract_host(url):
    extract = tldextract.extract(url)
    # if ip, only domain is set:
    if extract.domain and not extract.subdomain and not extract.suffix:
        return extract.domain
    return extract.fqdn


def analyze_website_headers(endpoint_id: int, url: int, protocol: str, headers: List[Dict[str, Any]]):
    # The HTTP specification RFC 7230 section 3.2 says header names are case-insensitive.

    """
    #125: CSP can replace X-XSS-Protection and X-Frame-Options. Thus if a (more modern) CSP header is present, assume
    that decisions have been made about what's in it and ignore the previously mentioned headers.

    We don't mandate CSP yet because it's utterly complex and therefore comes with an extremely low adoption ratio.

    https://stackoverflow.com/questions/43039706/replacing-x-frame-options-with-csp
    X-Frame-Options: SAMEORIGIN ➡ Content-Security-Policy: frame-ancestors 'self'

    https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
    X-XSS-Protection -> ('unsafe-inline')

    X-Content-Type-Options is not affected.
    """
    # normal headers only need to be present on the last page, whereas hsts needs to be present on each https page.
    last_redirect_headers = CaseInsensitiveDict(headers[-1]["headers"])

    # A complex mess, we're not requiring it, but at least we can grade it low or ok if need be.
    generic_check(endpoint_id, last_redirect_headers, "Content-Security-Policy")

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Permissions_Policy
    # This is based on an allowlist. Secure defaults?
    generic_check(endpoint_id, last_redirect_headers, "Permissions-Policy")

    # default since 2020 = strict-origin-when-cross-origin, which leaks data.
    # The privacy friendly default should be same-origin or no-referrer. So this should be set to a privacy
    # friendly level.
    generic_check(endpoint_id, last_redirect_headers, "Referrer-Policy")

    # We've removed conditional scans in scannerss, as more scan data is better.
    # you can cohose not to display or report it. Below used to be conditional scans.

    # x-xss is deprecated
    # generic_check_using_csp_fallback(endpoint_id, headers, "X-XSS-Protection")
    generic_check_using_csp_fallback(endpoint_id, last_redirect_headers, "X-Frame-Options")
    generic_check(endpoint_id, last_redirect_headers, "X-Content-Type-Options")

    """
    https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
    Note: The Strict-Transport-Security header is ignored by the browser when your site is accessed using HTTP;
    this is because an attacker may intercept HTTP connections and inject the header or remove it.  When your
    site is accessed over HTTPS with no certificate errors, the browser knows your site is HTTPS capable and will
    honor the Strict-Transport-Security header.

    7 march 2018
    After a series of complaints of products designed specifically to run on port 443 (without any non-secured port)
    we've decided that HSTS is only required if there are no unsecured (http) endpoints on this url.
    This means: if you still run (another) service on an unsecured http port, we're still demanding hsts. We're not
    able to see what product runs on what port (and it should be like that).

    The HSTS header is a sloppy patch job anyway, it will be phased out after browsers require sites to run on 443 and
    just start blocking port 80 for websites alltogether. On that day preloading also goes out the window.

    It basically works like this:
    A downgrade attack is not dependent on missing hsts. It's depending on who operates the network and if they employ
    DNS cache poisoning and use sslstrip etc. Then it comes down to your browser not accepting http at all either via
    preloading or the HSTS header (or design). The user may also trust your injected certificate given HPKP is removed
    everywhere and users will certainly click OK if they can.

    If you think it works differently, just file an issue or make a pull request. We want to get it right.

    July 5th 2023: the HSTS header is now mandatory by Dutch law, also on devices. We've had a lot of time to
    think about this header and it makes sense to apply it _even_ when there is no unsecure endpoint. This has
    to do with connecting to the site one a rogue network. Would HSTS be missing a downgrade attack is possible since
    there is nothing enforcing the browser to stay on the https website (except good behaviour by the browser).
    So as long as browsers allow connecting to port 80 this is an issue.
    Additionally HSTS has to be present on all websites in a redirect chain.

    The evidence we want:
    [
        {
            "request": 1,
            "url": "http://...".
            "headers": {}
        },
        {
            "request": 2,
            "url": "http://...".
            "headers": {}
        }
    ]
    """
    result = measure_hsts_header(headers)
    store_endpoint_scan_result(
        "http_security_header_strict_transport_security",
        endpoint_id,
        rating=result,
        message=result,
        evidence=json.dumps(prepare_headers_for_evidence(headers)),
    )

    return {"status": "success"}


def measure_hsts_header(headers):
    # at this point all https redirects or pages should have HSTS enabled, all xml-stuff has been filtered out before
    # this is required up until all browsers do not allow connection to port 80 (this should be behind a setting so
    # nobody uses port 80 in practice unless they really really want to).
    # Correction: every host needs to set the HSTS header once. So if there is a redirect that goes to the same host
    # and the second page sets HSTS this is fine. There is always an issue on first visit: that is not solved by setting
    # the HSTS header on the first or second response on the host: that does not matter as long as every host sets it.
    has_https = False
    # Dict with address and then if that host has HSTS
    hsts_header_per_https_host = {}
    for header_metadata in headers:
        host = extract_host(header_metadata["url"])

        if header_metadata["protocol"] == "https":
            has_https = True

            # default to false, and it should be there
            if host not in hsts_header_per_https_host:
                hsts_header_per_https_host[host] = False

            # correct it to true if hsts is present
            if "Strict-Transport-Security" in CaseInsensitiveDict(header_metadata["headers"]):
                hsts_header_per_https_host[host] = True

    # no https, means there is no reason to have this header.
    if not has_https:
        # clean up existing header metrics. HSTS is only needed on HTTPS connections.
        # HTTPS is mandatory nowadays, so this won't happen often
        return "not_applicable"

    for has_hsts in hsts_header_per_https_host.values():
        if not has_hsts:
            return "missing"

    # it was missing nowhere, so it is present wherever it was needed, and given one of the things was via https it
    # was needed!
    return "present"

    # the "missing_but_no_insecure_service_offered" is now removed as hsts is always needed on hostile networks
    # if protocol == "https":
    #     # runs any unsecured http service? (on ANY port).
    #     if Endpoint.objects.all().filter(url=url, protocol="http", is_dead=False).exists():
    #         generic_check(endpoint_id, headers, "Strict-Transport-Security")
    #     elif "Strict-Transport-Security" in headers:
    #         log.debug("Has Strict-Transport-Security")
    #         store_endpoint_scan_result(
    #             scan_type="http_security_header_strict_transport_security",
    #             endpoint_id=endpoint_id,
    #             rating="present",
    #             message="present",
    #             evidence=headers["Strict-Transport-Security"],
    #         )
    #     else:
    #         log.debug("Has no Strict-Transport-Security, yet offers no insecure http service.")
    #         # store_endpoint_scan_result(
    #         #     "http_security_header_strict_transport_security",
    #         #     endpoint_id,
    #         #     "missing_but_no_insecure_service_offered",
    #         #     "Security Header not present: Strict-Transport-Security, yet offers no insecure http service.",
    #         # )


def prepare_headers_for_evidence(headers: List[Dict[str, Any]]) -> str:
    # removes all headers that are not in the allow-list. This is because some headers just
    # change all the time such as Set-Cookie, Date, Content Length etc.
    # There will probably be more in the future. But for now this is fine, it covers all our tests and then some.
    #: Server: not included because load balancers will give you a different server every time.
    allowed_headers = [
        # remove Content security policy because it contains nonce values.
        # in fact, remove most of the other headers because the metric is about HSTS and other metrics
        # are already stored elsewhere
        # "content-security-policy",
        # "permissions-policy",
        # "referrer-policy",
        # "x-frame-options",
        # "content-type",
        # "x-clacks-overhead",
        # "cross-origin-opener-policy",
        # "x-content-type-options",
        # "referrer-policy",
        # keep location because it tells what page is requested next. Todo: reduce location links.
        "location",
        # "content-type",
        # "link",
        # "x-xss-protection",
        "strict-transport-security",
    ]
    prepared_headers = copy.deepcopy(headers)

    # todo: sort headers as well!
    for header_metadata in prepared_headers:
        # transform to lowercase headers, and then filter out what we don't need:
        header_metadata["headers"] = {k.lower(): v.lower() for k, v in header_metadata["headers"].items()}
        header_metadata["headers"] = {k: v for k, v in header_metadata["headers"].items() if k in allowed_headers}

        # sort the headers alphabetically
        header_metadata["headers"] = dict(sorted(header_metadata["headers"].items()))

        # reduce the location header if it is present to reduce the amount of data storage needed
        if "location" in header_metadata["headers"]:
            header_metadata["headers"]["location"] = reduce_url(header_metadata["headers"]["location"])

    return prepared_headers


def header_to_scan_type(header_name: str) -> str:
    return f"http_security_header_{header_name.lower().replace('-', '_')}"


def generic_check(endpoint_id: int, headers, header):
    # this is case insensitive

    scan_type = header_to_scan_type(header)

    if scan_type == "http_security_header_content_security_policy" and header in headers:
        headers[header] = clean_csp_header(headers[header])

    if header in headers.keys():
        store_endpoint_scan_result(
            scan_type=scan_type,
            endpoint_id=endpoint_id,
            rating="present",
            message="present",
            evidence=headers[header].strip(),
        )
    else:
        store_endpoint_scan_result(scan_type, endpoint_id, "missing", "missing")


def clean_csp_header(csp_header_value: str) -> str:
    # replace 'nonce-random with a standard string, preventing new records per measurement
    return re.sub(r"'nonce-[A-Za-z0-9].*?'", "'nonce-<TOKEN_OR_ID>'", csp_header_value)


def generic_check_using_csp_fallback(endpoint_id: int, headers, header):
    scan_type = f"http_security_header_{header.lower().replace('-', '_')}"

    # this is case insensitive
    if header in headers.keys():
        log.debug("Has %s", header)
        store_endpoint_scan_result(
            scan_type=scan_type,
            endpoint_id=endpoint_id,
            rating="present",
            message=headers[header],
            evidence=headers[header],
        )
    else:
        # CSP fallback:
        log.debug("CSP fallback used for %s", header)
        if "Content-Security-Policy" in headers.keys():
            store_endpoint_scan_result(
                scan_type=scan_type,
                endpoint_id=endpoint_id,
                rating="using_csp",
                message=f"Content-Security-Policy header found, which can handle the security from {header}. "
                f"Value (possibly truncated): {headers['Content-Security-Policy'][:80]}...",
                evidence=headers["Content-Security-Policy"],
            )

        else:
            log.debug("Has no %s", header)
            store_endpoint_scan_result(
                scan_type=scan_type,
                endpoint_id=endpoint_id,
                rating="missing",
                message=f"Security Header not present: {header}, alternative header "
                f"Content-Security-Policy not present.",
            )


def ensure_path(uri: str) -> str:
    """
    Make sure uri has a path component

    >>> ensure_path("http://example.com")
    'http://example.com/'

    >>> ensure_path("http://example.com/index.html")
    'http://example.com/index.html'

    """
    url_parts = list(urlsplit(uri))
    if url_parts[2] == "":
        url_parts[2] = "/"
        uri = urlunsplit(url_parts)
    return uri


# use timeout in case it starts downloading slow/long files. We don't want to wait.
# Placed inside separate function because the decorator will probably not match this method
# call / warp or distort the canvas or etc.
# A better solution should be looked at, perhaps using HEAD requests or stopping transfer after N bytes with urllib3
@app.task(time_limit=HEADER_RETRIEVAL_TRANSFER_TIMEOUT)
def get_headers(uri_url: str, host: str, user_agent: str) -> Union[List[Dict[str, Any]], bool]:
    """
    Issue #94:
    TL;DR: The fix is to follow all redirects.

    Citing: https://stackoverflow.com/questions/22077618/respect-x-frame-options-with-http-redirect
    Source: https://tools.ietf.org/html/rfc7034
    Thanks to: antoinet.

    From the terminology used in RFC 7034,

    The use of "X-Frame-Options" allows a web page from host B to declare that its content (for example, a
    button, links, text, etc.) must not be displayed in a frame (<frame> or <iframe>) of another page (e.g.,
    from host A). This is done by a policy declared in the HTTP header and enforced by browser implementations
    as documented here.

    The X-Frame-Options HTTP header field indicates a policy that specifies whether the browser should render
    the transmitted resource within a <frame> or an <iframe>. Servers can declare this policy in the header of
    their HTTP responses to prevent clickjacking attacks, which ensures that their content is not embedded
    into other pages or frames.


    Similarly, since a redirect is a flag not to render the content, the content can't be manipulated.
    This also means no X-XSS-Protection or X-Content-Type-Options are needed. So just follow all redirects.

    Update 17 dec 2018: some web servers require a user agent to be sent in order to give a "more correct" response.
    Given that 'humans with browsers' access these pages, it's normal to also send a user agent.

    :return: niquests.response
    """
    # ensure uri has a path part, otherwise this messes with responses/mock
    uri_url = ensure_path(uri_url)

    log.debug("Getting headers for %s, ipv4:%s, ipv6:%s", uri_url, not DISABLE_IPV4, not DISABLE_IPV6)

    # Not passing exceptions between functions.
    # ignore wrong certificates, those are handled in a different scan.
    # disable ipv4 or ipv6 depending on the type of scanner
    # using niquests instead of requests as it supports disabling v4/v6
    # 10 seconds for network delay, 10 seconds for the site to respond.
    # WHEN: Failed to parse headers (url=https://gvo.schiedam.nl:443/): [MissingHeaderBodySeparatorDefect()],
    #   unparsed data: 'Access-Control-Allow-Origin : https://gvo. <-- notice space before colon
    # THEN: urllib3.exceptions.HeaderParsingError still waits the full timeout, even though headers have been received.
    #   a read timeout occurs.
    try:
        with niquests.Session(disable_ipv4=DISABLE_IPV4, disable_ipv6=DISABLE_IPV6) as session:
            log.debug("Headers request for url: %s", uri_url)
            standard_headers = create_standard_request_headers(user_agent=user_agent)
            log.debug("Sent headers: %s", standard_headers)
            log.debug("Allow redirects: True, verify: False, stream: True")
            response = session.get(
                uri_url,
                timeout=HEADER_RETRIEVAL_TIMEOUT,
                allow_redirects=True,
                verify=False,  # nosec TLS does not have to be valid in this test, only headers do.
                stream=True,  # don't read the message body unless it is required
                # Redirects are allowed, so don't set the host header because that is set between requests
                # and a redirected server will not understand that response.
                headers=standard_headers,
            )

        # do not raise for status, the error pages should also set headers correctly.
        # response.raise_for_status()
    except niquests.RequestException as my_exception:
        log.debug("Could not request headers, exception: %s", my_exception)
        return False
    except BaseException:
        log.exception("Could not get headers for url: %s", uri_url)
        return False

    # redirects are followed, this gives an indication on how many redirects are followed, and what url the
    # headers are taken from. We need the headers of all sites in this history to make sure the HSTS header is
    # set everywhere.
    responses = []
    for index, resp in enumerate(response.history):
        responses.append(
            {
                "request": index + 1,
                "url": resp.url,
                "protocol": "https" if resp.url.startswith("https") else "http",
                "status_code": resp.status_code,
                # casing works fine as this is now a case insensitive dict.
                "content_type": resp.headers.get("Content-Type", "unknown").lower(),
                # case insensitive dicts are not serializable
                "headers": dict(resp.headers),
            }
        )
        log.debug("- Redirect %s: %s %s.", index, resp.status_code, resp.url)

    log.debug("- You are now at %s: %s.", response.status_code, response.url)

    # Removed: only continue for valid responses (eg: 200)
    # Error pages, such as 404 are super fancy, with forms and all kinds of content.
    # it's obvious that such pages (with content) follow the same security rules as any 2XX response.
    # if 400 <= response.status_code <= 600:
    #     error_response_400_500(endpoint)
    # response.raise_for_status()

    for header in response.headers:
        log.debug("Received header: %s", header)

    # and the final response:
    responses.append(
        {
            "request": len(responses) + 1,
            "url": reduce_url(response.url),
            "protocol": response.url.startswith("https") and "https" or "http",
            "status_code": response.status_code,
            "content_type": response.headers.get("Content-Type", "unknown").lower(),
            # case insensitive dicts are not serializable
            "headers": dict(response.headers),
        }
    )

    return responses


def reduce_url(url: str) -> str:
    # urlparse, get the address and entire path
    clean_url = urlparse(url)

    # strings such as "nonsense" in urlparse create: ://nonsense
    if not clean_url.scheme:
        return ""

    new_url = f"{clean_url.scheme}://{clean_url.netloc}{clean_url.path}"

    # then remove GUIDS from that path and replace them with <GUID>
    new_url = remove_random_identifiers(new_url)

    # then make sure that an url is not longer than 255 chars, because come on...
    return new_url[:255]


def remove_guid_from_string(string: str) -> str:
    # does not work with
    # this:       d32e16a3-1474 4861         a954            cc481b8aa509
    # The guid regex below might only match "legit" guids, but there are enough sites that DO NOT use real guids
    # so the regex has been simplified to match any hex sequence without the special cases
    # original regex: r"[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
    # While official guids don't have uppercase characters, that's now how you find them on the internet
    return re.sub(r"[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", "GUID", string)


def remove_timestamp_from_end_of_string(string: str) -> str:
    # Any timestamp between 2016 and 2042 is a number between 1451606400 and 2272147200
    # timestamps are used usually in urls to invalidate or bypass caching and appended to a url.
    return re.sub(r"[1-2][0-9]{9}$", "TIMESTAMP", string)


def remove_random_identifiers(string: str) -> str:
    # this is heuristics based
    string = remove_guid_from_string(string)
    string = re.sub(r"jsessionid=[A-Za-z0-9]*", "jsessionid=<TOKEN_OR_ID>", string)
    string = re.sub(r"crypt=[A-Za-z0-9]*", "crypt=<TOKEN_OR_ID>", string)

    # this basiscally contains all cookies, user agent and whatever dynamic data is found on the site.
    # so this will be extremely dynamic. Just strip everything and replace it with a placeholder for the dynamic data.
    string = re.sub(
        r"https:\/\/region1\.google-analytics\.com/g/collect?.*",
        "https://region1.google-analytics.com/g/collect?<TRACKING_DATA>",
        string,
    )

    return string


def get_hsts_chain_impression():
    layer = OrganizationType.objects.get(name="municipality")
    scans = (
        EndpointGenericScan.objects.all()
        .filter(
            type="http_security_header_strict_transport_security",
            is_the_latest_scan=True,
            endpoint__is_dead=False,
            endpoint__url__is_dead=False,
            endpoint__url__not_resolvable=False,
            endpoint__url__organization__is_dead=False,
            endpoint__url__organization__layers=layer,
        )
        .only("id", "endpoint", "evidence")
        .order_by("id")
    )
    for scan in scans:
        with contextlib.suppress(Exception):
            invalid_redirects = hsts_chain_validate(json.loads(scan.evidence))
            if invalid_redirects:
                print(scan.endpoint)
                print(invalid_redirects)


def hsts_chain_validate(hsts_chain: List[Dict[str, Union[str, int]]]) -> List[Any]:
    """
    HSTS has to first upgrade on the _SAME_DOMAIN_ and then redirect to another domain.
    It's not allowed to redirect to another domain from HSTS.
    "een bezoeker wordt doorverwezen naar de HTTPS-versie op de bezochte domeinnaam voordat naar andere
    domeinnamen wordt doorverwezen overeenkomstig maatregel 5 bij beveiligingsrichtlijn U/WA.05 van de
    Webapplicatie-richtlijnen; "
    "bezochte domeinnaam, dus niet subdomeinenaam! ook niet poort"
    [
        {"request": 1, "url": "https://educator.windesheim.nl:443/", "protocol": "https", "status_code": 302,
            "content_type": "unknown", "headers": {"location": "", "strict-transport-security": "max-age=31536000"}},
        {"request": 2, "url": "https://educator.windesheim.nl:443/home", "protocol": "https", "status_code": 302,
            "content_type": "unknown", "headers": {"location": "", "strict-transport-security": "max-age=31536000"}},
        {"request": 3, "url": "https://educator.windesheim.nl:443/login", "protocol": "https", "status_code": 302,
            "content_type": "unknown", "headers": {"location": "", "strict-transport-security": "max-age=31536000"}},
        {"request": 4, "url": "https://educator.windesheim.nl:443/saml/login", "protocol": "https", "status_code": 302,
            "content_type": "unknown", "headers": {"location": "https://sts.windesheim.nl/adfs/ls/",
            "strict-transport-security": "max-age=31536000"}},
        {"request": 5, "url": "https://sts.windesheim.nl/adfs/ls/", "protocol": "https", "status_code": 200,
            "content_type": "text/html; charset=utf-8", "headers": {}}]
    """
    invalid_forwards = []
    previous_request = None
    for request in hsts_chain:
        if not previous_request:
            previous_request = request
            continue
        previous_extract = tldextract.extract(previous_request["url"])
        current_extract = tldextract.extract(request["url"])
        if previous_request["protocol"] == "http" and previous_extract.domain not in current_extract.domain:
            invalid_forwards.append({"from": previous_extract.fqdn, "to": current_extract.fqdn})
        previous_request = request
    return invalid_forwards


def get_suggested_sites():
    # from websecmap.scanners.scanner.security_headers import *
    # import contextlib
    # from pprint import pprint
    # pprint(sug)
    # it results not just in nice redirects but also sites that are obviously wrong :)
    # these are suppliers
    useless_subdomain = [
        "accounts.google.com",
        "login.windows.net",
        "outlook.office365.com",
        "login.microsoftonline.com",
        "sentby.spotler.com",
        "m.youtube.com",
    ]
    vendor_domain = [
        "simiam.nl",
        "digid.nl",
        "outlook.com",
        "eherkenning.nl",
        "somtoday.nl",
        "vmwareidentity.de",
        "sharepoint.com",
        "surfconext.nl",
        "leefbaarometer.nl",
        "overheid.nl",
        "b2clogin.com",
        "azurewebsites.net",
        "moo.nl",
        "akamai-access.com",
        "youtube.com",
        "signhost.com",
        "rvig.nl",
        "rijksoverheid.nl",
        "meierijstad.nl",
    ]
    scans = (
        EndpointGenericScan.objects.all()
        .filter(
            type="http_security_header_strict_transport_security",
            is_the_latest_scan=True,
            endpoint__is_dead=False,
            # prevent tons of duplicates:
            # endpoint__port=443,
            # endpoint__ip_version=4,
            endpoint__url__is_dead=False,
            endpoint__url__not_resolvable=False,
        )
        .only("id", "endpoint", "evidence", "endpoint__url__url")
        .order_by("-id")
    )
    suggestions = {}
    for scan in scans:
        with contextlib.suppress(Exception):
            hsts_chain = json.loads(scan.evidence)
            for request in hsts_chain:
                current_extract = tldextract.extract(request["url"])
                if current_extract.fqdn in useless_subdomain:
                    continue
                if f"{current_extract.domain}.{current_extract.suffix}" in vendor_domain:
                    continue
                orig = tldextract.extract(scan.endpoint.url.url)
                if current_extract.domain != orig.domain:
                    print(f"Suggested site: {current_extract.fqdn} for {scan.endpoint.url.url}")
                    if current_extract.fqdn not in suggestions:
                        suggestions[current_extract.fqdn] = set()
                    suggestions[current_extract.fqdn].add(scan.endpoint.id)
    return suggestions
