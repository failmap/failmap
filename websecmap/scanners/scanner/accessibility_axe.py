from collections import defaultdict
import json
import logging

from websecmap.app.models import Translation
from websecmap.reporting.models import ScanPolicy
from websecmap.scanners.models import Scanner, ScanType, ScanTypeCategory, ScanTypeDocumentationLink
from websecmap.scanners.scanmanager import store_endpoint_scan_result

from websecmap.scanners.scanner.accessibility_axe_rules import rules_json
from websecmap.scanners_accessibility.models import ScanSession
from websecmap.scanners.models import Endpoint

log = logging.getLogger(__name__)

rules_by_key = {rule["rule"]: rule for rule in rules_json}


# todo: scan/plan etc as per usual...


def create_translation_scaffold():
    """
    accessibility_axe_color_contrast": {
            "explanation": "This rule checks that the highest possible contrast of every text .",
            "rating": {
                "accessibility_axe_ok": "No contrast issues detected (anymore)",
                "accessibility_axe_serious": "Insufficient color contrast measured"
            },
            "technology": "Accessibility",
            "title": "Ensure the contrast between foreground and background colors meets"
        },
    """
    translations = {}
    for _, rule in enumerate(rules_json, start=4000):
        key = f"accessibility_axe_{rule['rule'].replace('-', '_')}"
        impact = rule["impact"].lower()
        translations[key] = {
            "explanation": "todo",
            "rating": {
                "accessibility_axe_ok": "ok (todo: full description)",
                f"accessibility_axe_{impact}": f"{impact} (todo: full description)",
            },
            "technology": "Accessibility",
            "title": rule["description"],
        }

    # sort translations by key:
    translations = dict(sorted(translations.items()))

    print(json.dumps(translations, indent=4))


def import_axe_scan_types():
    # create axe scanner if not exists, then create the scan types
    scanner = Scanner.objects.all().filter(python_name="accessibility_axe").first()

    if not scanner:
        scanner = Scanner.objects.create(
            python_name="accessibility_axe",
            name="Accessibility Axe Scanner",
            enabled=False,
        )

    accessibility_category, _ = ScanTypeCategory.objects.get_or_create(name="accessibility")

    # only _append_ new scan types and you're fine :)
    for order, rule in enumerate(rules_json, start=4000):
        scan_type, _ = ScanType.objects.get_or_create(
            name=f"accessibility_axe_{rule['rule'].replace('-', '_')}",
            description=rule["description"],
            # use whatever is the default here, otherwise a new record is created.
            # show_in_frontend=False,
            # include_in_report=False,
        )

        if not scan_type.display_order:
            scan_type.display_order = order
            scan_type.save()

        scan_type.category.add(accessibility_category)

        # make nice tabs per category
        for tag in rule["tags"]:
            safe_tag_name = tag.replace(" ", "_").replace(".", "_").replace("-", "_")
            safe_tag_name = safe_tag_name.replace("cat_", "category_")

            # if safe_tag_name in ["ttv5", "en_301_549", "wcag2a", "wcag2aa", "wcag2aaa", "wcag21aa", "section508"]:

            # add some additional categories per compliance, don't tag the individual chapter
            if safe_tag_name.lower().startswith("tt"):
                if safe_tag_name in ["TTv5"]:
                    tmp_acc_cat, _ = ScanTypeCategory.objects.get_or_create(name=f"compliance_{safe_tag_name}")
                    scan_type.category.add(tmp_acc_cat)
                continue
            if safe_tag_name.lower().startswith("section508"):
                if safe_tag_name in ["section508"]:
                    tmp_acc_cat, _ = ScanTypeCategory.objects.get_or_create(name=f"compliance_{safe_tag_name}")
                    scan_type.category.add(tmp_acc_cat)
                continue
            if safe_tag_name.lower().startswith("en"):
                if safe_tag_name in ["EN_301_549"]:
                    tmp_acc_cat, _ = ScanTypeCategory.objects.get_or_create(name=f"compliance_{safe_tag_name}")
                    scan_type.category.add(tmp_acc_cat)
                continue
            if safe_tag_name.lower().startswith("wcag"):
                if safe_tag_name in ["wcag2a", "wcag2aa", "wcag2aaa", "wcag21a", "wcag21aa"]:
                    tmp_acc_cat, _ = ScanTypeCategory.objects.get_or_create(name=f"compliance_{safe_tag_name}")
                    scan_type.category.add(tmp_acc_cat)
                continue
            if safe_tag_name.lower().startswith("act"):
                if safe_tag_name in ["ACT"]:
                    tmp_acc_cat, _ = ScanTypeCategory.objects.get_or_create(name=f"compliance_{safe_tag_name}")
                    scan_type.category.add(tmp_acc_cat)
                continue

            log.debug("Adding %s to scan_type %s", safe_tag_name, scan_type)
            tmp_accessibility_category, _ = ScanTypeCategory.objects.get_or_create(name=safe_tag_name)
            scan_type.category.add(tmp_accessibility_category)

        scanner.creates_scan_types.add(scan_type)

        # add the act_rules as documentation links
        for act_rule in rule["act_rules"]:
            documentation_link, _ = ScanTypeDocumentationLink.objects.get_or_create(
                provider=f"act_rules_community_{act_rule['text']}",
                friendly_name=f"ACT Rule ({act_rule['text']})",
                url=act_rule["url"],
            )
            scan_type.documentation_links.add(documentation_link)

        # also add the promotional link to deque university
        # this might be considered spam, and perhaps this should be on the finding itself instead of elsewhere
        documentation_link, _ = ScanTypeDocumentationLink.objects.get_or_create(
            provider="deque_university", friendly_name="Deque University", url=rule["link"]
        )
        scan_type.documentation_links.add(documentation_link)

        # add policies
        impact = rule["impact"].lower()
        policy, _ = ScanPolicy.objects.get_or_create(
            scan_type=scan_type.name,
            conclusion=f"accessibility_axe_{impact}",
            explanation=f"accessibility_axe_{impact}",
        )
        if impact in ["critical", "serious"]:
            policy.high = 1
            policy.medium = 0
            policy.low = 0

        if impact in ["moderate"]:
            policy.high = 0
            policy.medium = 1
            policy.low = 0

        if impact in ["minor"]:
            policy.high = 0
            policy.medium = 0
            policy.low = 1

        policy.save()

        # also add OK policy
        policy, _ = ScanPolicy.objects.get_or_create(
            scan_type=scan_type.name,
            conclusion="accessibility_axe_ok",
            explanation="accessibility_axe_ok",
            ok=1,
        )
        policy.save()


def import_all_existing_violations():
    for scan_session in ScanSession.objects.all().filter(violation_count__gte=1):
        for violations_file in scan_session.violations.all():
            with open(violations_file.file_object.path, "r", encoding="UTF-8") as f:
                found_violations = json.load(f)
                # todo: scan session is on url, but should this be done on endpoint? Perhaps not, why would there be
                #  a different website on different endpoints except when there is a configuration mistake...
                endpoint = Endpoint.objects.all().filter(url=scan_session.url).first()
                store_axe_results(endpoint.id, found_violations)


def store_axe_results(endpoint_id, axe_results):
    # axe results is the result of the "violations" key in the results
    # todo: things can also be OK etc...
    clean_axe_violations_inline(axe_results)
    violations_per_id = categorize_axe_results(axe_results)
    # todo: store the story in the meaning field.
    for violation_id, violations in violations_per_id.items():
        if violation_id not in rules_by_key:
            log.error("Unknown axe violation: %s", violation_id)
            continue
        store_endpoint_scan_result(
            scan_type=f'accessibility_axe_{violation_id.replace("-", "_")}',
            endpoint_id=endpoint_id,
            rating=f"accessibility_axe_{rules_by_key[violation_id]['impact'].lower()}",
            message="",
            evidence=json.dumps(violations),
        )


def categorize_axe_results(axe_results):
    """
    There is a risk that the report will contain malicious stuff, or at least data we don't want in there.
    Of course our target audience is pretty nice, but they can have html elements in swearwords etc. :)

    It's not clear what nodes have been gathered on what page / what step in the process. Which makes the results
    # very hard to understand. "Where do i have to apply the fix?" is the first question.

    Will not currently deduplicate nodes, there might be duplicates due to menu's headers and footers.

    An axe violation looks like this and has a ton of noded per page.
    {
      "id": "color-contrast",
      "impact": "serious",
      "tags": [
        "cat.color",
        "wcag2aa",
        "wcag143",
        "TTv5",
        "TT13.c",
        "EN-301-549",
        "EN-9.1.4.3",
        "ACT"
      ],
      "description": "Ensure the contrast between foreground and background colors meets WCAG 2 AA...",
      "help": "Elements must meet minimum color contrast ratio thresholds",
      "helpUrl": "https://dequeuniversity.com/rules/axe/4.10/color-contrast?application=axeAPI",
      "nodes": [
        {
          "any": [
            {
              "id": "color-contrast",
              "data": {
                "fgColor": "#4acb0e",
                "bgColor": "#fafbfc",
                "contrastRatio": 2.05,
                "fontSize": "12.0pt (16px)",
                "fontWeight": "normal",
                "messageKey": null,
                "expectedContrastRatio": "4.5:1",
                "shadowColor": null
              },
              "relatedNodes": [
                {
                  "html": "<body class=\"page-template-default page page-id-30 page-parent wp-custom-logo wp-embed ...
                  "target": [
                    "body"
                  ]
                }
              ],
              "impact": "serious",
              "message": "Element has insufficient color contrast of 2.05 (foreground color: #4acb0e, background ...
            }
          ],
          "all": [],
          "none": [],
          "impact": "serious",
          "html": "<a href=\"https://internetcleanup.foundation/2023/08/nieuwe-meting-dienstverlening-binnen-de-...
          "target": [
            "p:nth-child(5) > a:nth-child(2)"
          ],
          "failureSummary": "Fix any of the following:\n  Element has insufficient color contrast of 2.05 (...
        }
      ]
    },
    """
    # split the results per violation id. These become separate metrics.
    # the path towards the violation is needed(!)
    # doe je al deduplicatie ergens? Dat is niet nodig.
    violations_per_id = defaultdict(list)
    for violation in axe_results:
        # todo: the path has to be added to the violation somewhere, otherwise the results are hard to replicate
        # todo: deduplicate the violations over all pages, so that only the first occurrence is kept.
        violations_per_id[violation["id"]].append(
            {
                # the path is now a free form. It should explain how we got to this page. Though clicking
                # or whatever. It may also be a pointer to an overall set of steps. Anything that answers the question
                # on how we got here / where should this issue be fixed.
                "path": ["/", "clicked some page"],
                # adding a title can be very helpful for finding the location of the issue.
                "title": "",
                # todo: at nodes add "is_duplicate" and a hash of the node for quick comparison.
                # why: duplicates are important, but in some scenario's they might be annoying as there are too many
                # that do not add anything.
                "violations": violation["nodes"],
            }
        )
    return violations_per_id


def clean_axe_violations_inline(axe_violations):
    # a check is performed per page, this means that certain recurring items like a header, menu and footer
    """
    Impact, tags, description, help, helpUrl are always the same for all "id": "color-contrast", violations.
    Only the ID needs to be present in the report.
      "id": "color-contrast",
      "impact": "serious",
      "tags": [
        "cat.color",
        "wcag2aa",
        "wcag143",
        "TTv5",
        "TT13.c",
        "EN-301-549",
        "EN-9.1.4.3",
        "ACT"
      ],
      "description": "Ensure the contrast between foreground and background colors meets WCAG 2 AA...",
      "help": "Elements must meet minimum color contrast ratio thresholds",
      "helpUrl": "https://dequeuniversity.com/rules/axe/4.10/color-contrast?application=axeAPI",
    """

    # deletion is inline, don't need to return a new report.
    for violation in axe_violations:
        # impact differs it seems, so just keep that...
        # del(violation["impact"])
        del violation["tags"]
        del violation["description"]
        del violation["help"]
        del violation["helpUrl"]


def category_translations():

    all_translations = {
        "category_color": {"en": "📐 Color", "nl": "📐 Kleuren"},
        "category_keyboard": {"en": "📐 Keyboard", "nl": "📐 Toetsenbord"},
        "category_time_and_media": {"en": "📐 Time and media", "nl": "📐 Tijd en media"},
        "category_name_role_value": {"en": "📐 Name Role Value", "nl": "📐 Naam, rol en waarde"},
        "category_aria": {"en": "📐 ARIA", "nl": "📐 ARIA"},
        "category_text_alternatives": {"en": "📐 Text alternatives", "nl": "📐 Tekstalternatieven"},
        "category_semantics": {"en": "📐 Semantics", "nl": "📐 Semantiek"},
        "category_tables": {"en": "📐 Tables", "nl": "📐 Tabellen"},
        "category_sensory_and_visual_cues": {
            "en": "📐 Sensory and visual cues",
            "nl": "📐 Sensorisch en visuele aanwijzingen",
        },
        "category_language": {"en": "📐 Language", "nl": "📐 Taal"},
        "category_forms": {"en": "📐 Forms", "nl": "📐 Formulieren"},
        "category_parsing": {"en": "📐 Parsing", "nl": "📐 Verwerken"},
        "category_structure": {"en": "📐 Structure", "nl": "📐 Structuur"},
        "compliance_wcag2a": {"en": "📒 WCAG 2.0 A", "nl": "📒 WCAG 2.0 A"},
        "compliance_wcag2aa": {"en": "📒 WCAG 2.0 AA", "nl": "📒 WCAG 2.0 AA"},
        "compliance_wcag2aaa": {"en": "📒 WCAG 2.0 AAA", "nl": "📒 WCAG 2.0 AAA"},
        "compliance_wcag21a": {"en": "📒 WCAG 2.1 A", "nl": "📒 WCAG 2.1 A"},
        "compliance_wcag21aa": {"en": "📒 WCAG 2.1 AA", "nl": "📒 WCAG 2.1 AA"},
        "compliance_ACT": {"en": "📒 ACT", "nl": "📒 ACT"},
        "compliance_EN_301_549": {"en": "📒 EN-301-549", "nl": "📒 EN-301-549"},
        "compliance_TTv5": {"en": "📒 TTv5", "nl": "📒 TTv5"},
        "compliance_section508": {"en": "📒 Section 508", "nl": "📒 Section 508"},
        "review_item": {"en": "📒 Review Item", "nl": "📒 Controlepunt"},
        "best_practice": {"en": "📒 Best Practice", "nl": "📒 Best Practice"},
        "experimental": {"en": "📒 Experimental", "nl": "📒 Experimenteel"},
    }

    for key, translation in all_translations.items():
        print(key)
        translation = Translation.objects.create(
            key=key,
            language_code="en",
            value=translation["en"],
        )
        translation.metadata_section.add("category")
