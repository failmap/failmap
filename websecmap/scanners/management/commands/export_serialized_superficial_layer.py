import logging

from django.core.management.base import BaseCommand

from websecmap.scanners.housekeeping.export_serialized.organization import export_superficial_layer
from websecmap.organizations.models import OrganizationType

log = logging.getLogger(__package__)


class Command(BaseCommand):
    help = "A superficial export of a layer: contains layers, organizations, coordinates and urls. No metrics."

    def add_arguments(self, parser):
        parser.add_argument("layer_name", help="Name of the layer to export")
        super().add_arguments(parser)

    def handle(self, *args, **options):
        print(export_superficial_layer(OrganizationType.objects.get(name=options["layer_name"].lower())))
