import logging

from django.core.management.base import BaseCommand

from websecmap.scanners.housekeeping.tasks import summarize_housekeeping_results

log = logging.getLogger(__package__)


class Command(BaseCommand):
    help = "Inspection tool to see how well housekeeping works."

    def add_arguments(self, parser):
        parser.add_argument("organization_id", help="Organization to view housekeeping results for.")
        super().add_arguments(parser)

    def handle(self, *args, **options):
        print(summarize_housekeeping_results(options["organization_id"]))
