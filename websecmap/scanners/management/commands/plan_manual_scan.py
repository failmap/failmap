import logging
from datetime import datetime, timezone

from django.core.management.base import BaseCommand

from websecmap.organizations.models import Url
from websecmap.scanners.models import PlannedScan, State, Activity, Scanner

log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    Create a planned scan record for a specific url.
    """

    # add url option
    def add_arguments(self, parser):
        scan_choices = Scanner.objects.values_list("python_name", flat=True)
        parser.add_argument("url", help="URL to scan")
        parser.add_argument("scanner", choices=scan_choices, help="Scanner to use")
        super().add_arguments(parser)

    def handle(self, *args, **options):
        # internet.nl v2 scanner has to be used in websecmap.

        url = Url.objects.all().filter(url=options["url"]).first()
        if not url:
            log.error("No URL found for %s", options["url"])
            return

        created = PlannedScan.objects.create(
            url=url,
            scanner=Scanner.objects.all().filter(python_name=options["scanner"]).first(),
            requested_at_when=datetime.now(timezone.utc),
            last_state_change_at=datetime.now(timezone.utc),
            state=State.requested,
            activity=Activity.scan,
            origin="Command line request",
        )

        log.info(
            "Planned scan %s created. Url: %s Scanner %s. Scan will be picked up.",
            created.pk,
            options["url"],
            options["scanner"],
        )
        # for when logging is disabled:
        print(
            f"Planned scan {created.pk} created. Url: {options['url']} Scanner: {options['scanner']}."
            f" Scan will be picked up."
        )
