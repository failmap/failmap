# Housekeeping

## What does it do
Clean up the database by reducing test-data, merging endpoints and merging duplicate tests on these endpoints.

You can call housekeeping commands directly as a scanner, so you can housekeep on individual organizations.

## Creating testdata for housekeeping
Using export_serialized commands you can create exports for entire organizations. These can be imported into a local
websecmap instance for testing purposes.

How to create an export of organization 1337, the Internet Cleanup Foundation.
```sh
  websecmap export_serialized_organization 1337 > /home/stitch/housekeeping_export_1337_internet_cleanup_foundation.json
```

For efficiency apply xz compression to these fixtures when shipping these inside the source of wsm.
Use xz -9 for this. It reduces a 40 meg file to 600 kilobyte. Or the 2 gigabyte file from Amsterdam to just 17 megabyte.


To load this data locally:
```sh
websecmap loaddata housekeeping_export_1337_internet_cleanup_foundation.json.xz
Loading data from fixtures - disabling foreign key checks
Installed 36223 object(s) from 1 fixture(s)

websecmap loaddata housekeeping_export_52_gemeente_breda.json.xz
Loading data from fixtures - disabling foreign key checks
Installed 86064 object(s) from 1 fixture(s)

websecmap loaddata housekeeping_export_134_gemeente_hardinxveld_giessendam.json.xz
Loading data from fixtures - disabling foreign key checks
Installed 21720 object(s) from 1 fixture(s)

websecmap loaddata housekeeping_export_385_gemeente_zutphen.json.xz
Loading data from fixtures - disabling foreign key checks
Installed 49288 object(s) from 1 fixture(s)

websecmap loaddata housekeeping_export_4084_gemeente_amsterdam.json.xz
Loading data from fixtures - disabling foreign key checks
Installed 606830 object(s) from 1 fixture(s)
```

## Cleaning sample data locally
Then you can run:

```sh
websecmap scan housekeeping -o "Internet Cleanup Foundation"
websecmap scan housekeeping -o "Gemeente Breda"
websecmap scan housekeeping -o "Gemeente Zutphen"
websecmap scan housekeeping -o "Gemeente Hardinxveld-Giessendam"
websecmap scan housekeeping -o "Gemeente Amsterdam"
```

This will create a log of debug logging.
# todo: remove some of the logging.sh

Afterwards you can see new housekeeping records per url here:

http://localhost:8000/admin/scanners/urlgenericscan/


## Summarizing inspections

Use the following command to summarize the cleanups, to get a gist on how effective housekeeping was.

```sh
websecmap summarize_housekeeping_results 1337
websecmap summarize_housekeeping_results 52
websecmap summarize_housekeeping_results 134
websecmap summarize_housekeeping_results 385
websecmap summarize_housekeeping_results 4084
```
