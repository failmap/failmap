import json
from datetime import timezone, datetime

from freezegun import freeze_time

from websecmap.organizations.models import (
    Url,
    Organization,
    OrganizationType,
    AlternativeName,
    Coordinate,
    VerifiedDomainRegistrant,
    OrganizationSurrogateId,
)
from websecmap.scanners.housekeeping.export_serialized.organization import export_url, export_organization
from websecmap.scanners.models import Endpoint, EndpointGenericScan, UrlGenericScan


@freeze_time("2025-03-02")
def test_export_url(db):
    u = Url.objects.create(url="example.com")

    data = export_url(u)

    assert json.loads(data) == [
        {
            "fields": {
                "computed_domain": "example",
                "computed_subdomain": "",
                "computed_suffix": "com",
                "contains_website_redirect": False,
                "created_on": "2025-03-02T00:00:00Z",
                "dns_supports_mx": False,
                "do_not_find_subdomains": False,
                "internal_notes": None,
                "is_dead": False,
                "is_dead_reason": None,
                "is_dead_since": None,
                "not_resolvable": False,
                "not_resolvable_reason": None,
                "not_resolvable_since": None,
                "onboarded": False,
                "onboarded_on": None,
                "onboarding_stage": None,
                "onboarding_stage_set_on": None,
                "organization": [],
                "origin": "",
                "subdomain_iregexes_to_ignore": "[]",
                "url": "example.com",
                "uses_dns_wildcard": False,
            },
            "model": "organizations.url",
            "pk": u.pk,
        },
    ]


@freeze_time("2025-03-02")
def test_export_organization(db):
    ot = OrganizationType.objects.create(name="test")
    an = AlternativeName.objects.create(name="testname")

    o = Organization.objects.create(name="testorganization")
    o.layers.add(ot)
    o.alternative_names.add(an)

    u = Url.objects.create(url="example.com")
    u.organization.add(o)

    c = Coordinate.objects.create(organization=o, geojsontype="point", area=[52, 4])
    osid = OrganizationSurrogateId.objects.create(organization=o, surrogate_id="test")
    vr = VerifiedDomainRegistrant.objects.create(organization=o, registrant_name="test")

    data = export_organization(o)
    assert json.loads(data) == [
        {
            "fields": {
                "description": None,
                "name": "test",
            },
            "model": "organizations.organizationtype",
            "pk": ot.id,
        },
        {
            "fields": {
                "description": None,
                "name": "testname",
            },
            "model": "organizations.alternativename",
            "pk": an.id,
        },
        {
            "fields": {
                "acceptable_domain_registrants": "[]",
                "alternative_names": [
                    an.id,
                ],
                "arbitrary_kv_data": "{}",
                "city": "",
                "computed_domain_registrant_statistics": "{}",
                "computed_name_slug": "testorganization",
                "country": "",
                "created_on": "2016-01-01T00:00:00Z",
                "internal_notes": None,
                "is_dead": False,
                "is_dead_reason": None,
                "is_dead_since": None,
                "layers": [
                    ot.id,
                ],
                "name": "testorganization",
                "reasoning_for_registrant_decisions": "",
                "structured_reasoning_for_registrant_decisions": "[\n"
                "    {\n"
                '        "registrant": "",\n'
                '        "reason": "empty registrant"\n'
                "    }\n"
                "]",
                "surrogate_id": None,
                "twitter_handle": None,
                "wikidata": None,
                "wikipedia": None,
                "written_address": "",
            },
            "model": "organizations.organization",
            "pk": o.id,
        },
        {
            "fields": {
                "area": "[\n" "    52,\n" "    4\n" "]",
                "calculated_area_hash": "c664445491b61b2005f9252fdcf741ad",
                "created_on": "2016-01-01T00:00:00Z",
                "creation_metadata": None,
                "edit_area": None,
                "geojsontype": "point",
                "is_dead": False,
                "is_dead_reason": None,
                "is_dead_since": None,
                "organization": o.id,
            },
            "model": "organizations.coordinate",
            "pk": c.id,
        },
        {
            "fields": {
                "admin_email_address": "",
                "is_still_valid": True,
                "notes": "",
                "organization": o.id,
                "registrant_name": "test",
            },
            "model": "organizations.verifieddomainregistrant",
            "pk": vr.id,
        },
        {
            "fields": {
                "organization": o.id,
                "surrogate_id": "test",
                "surrogate_id_name": None,
                "used_in_dataset": None,
            },
            "model": "organizations.organizationsurrogateid",
            "pk": osid.id,
        },
        {
            "fields": {
                "computed_domain": "example",
                "computed_subdomain": "",
                "computed_suffix": "com",
                "contains_website_redirect": False,
                "created_on": "2025-03-02T00:00:00Z",
                "dns_supports_mx": False,
                "do_not_find_subdomains": False,
                "internal_notes": None,
                "is_dead": False,
                "is_dead_reason": None,
                "is_dead_since": None,
                "not_resolvable": False,
                "not_resolvable_reason": None,
                "not_resolvable_since": None,
                "onboarded": False,
                "onboarded_on": None,
                "onboarding_stage": None,
                "onboarding_stage_set_on": None,
                "organization": [
                    o.id,
                ],
                "origin": "",
                "subdomain_iregexes_to_ignore": "[]",
                "url": "example.com",
                "uses_dns_wildcard": False,
            },
            "model": "organizations.url",
            "pk": u.id,
        },
    ]


@freeze_time("2025-03-02")
def test_export_url_with_relations(db):
    u = Url.objects.create(url="example.com")
    ep = Endpoint.objects.create(url=u, protocol="http", port="443", discovered_on=datetime.now(timezone.utc))
    epgs = EndpointGenericScan.objects.create(
        type="nuclei_exposed_panels",
        rating="info",
        evidence="test",
        endpoint=ep,
        rating_determined_on=datetime.now(timezone.utc),
    )
    ugs = UrlGenericScan.objects.create(
        type="nuclei_exposed_panels",
        rating="info",
        evidence="test",
        url=u,
        rating_determined_on=datetime.now(timezone.utc),
    )

    data = export_url(u)

    assert json.loads(data) == [
        {
            "fields": {
                "computed_domain": "example",
                "computed_subdomain": "",
                "computed_suffix": "com",
                "contains_website_redirect": False,
                "created_on": "2025-03-02T00:00:00Z",
                "dns_supports_mx": False,
                "do_not_find_subdomains": False,
                "internal_notes": None,
                "is_dead": False,
                "is_dead_reason": None,
                "is_dead_since": None,
                "not_resolvable": False,
                "not_resolvable_reason": None,
                "not_resolvable_since": None,
                "onboarded": False,
                "onboarded_on": None,
                "onboarding_stage": None,
                "onboarding_stage_set_on": None,
                "organization": [],
                "origin": "",
                "subdomain_iregexes_to_ignore": "[]",
                "url": "example.com",
                "uses_dns_wildcard": False,
            },
            "model": "organizations.url",
            "pk": u.pk,
        },
        {
            "fields": {
                "discovered_on": "2025-03-02T00:00:00Z",
                "ip_version": 4,
                "is_dead": False,
                "is_dead_reason": None,
                "is_dead_since": None,
                "origin": None,
                "port": 443,
                "protocol": "http",
                "security_policies": [],
                "url": u.pk,
            },
            "model": "scanners.endpoint",
            "pk": ep.pk,
        },
        {
            "fields": {
                "comply_or_explain_case_additional_notes": "",
                "comply_or_explain_case_handled_by": "",
                "comply_or_explain_explained_by": "",
                "comply_or_explain_explained_on": None,
                "comply_or_explain_explanation": "",
                "comply_or_explain_explanation_valid_until": None,
                "comply_or_explain_is_explained": False,
                "evidence": "test",
                "explanation": "0",
                "is_the_latest_scan": False,
                "last_scan_moment": "2025-03-02T00:00:00Z",
                "meaning": "{}",
                "prr_is_progress": False,
                "prr_is_regression": False,
                "prr_was_previously": 3,
                "rating": "info",
                "rating_determined_on": "2025-03-02T00:00:00Z",
                "type": "nuclei_exposed_panels",
                "url": u.pk,
            },
            "model": "scanners.urlgenericscan",
            "pk": ugs.pk,
        },
        {
            "fields": {
                "comply_or_explain_case_additional_notes": "",
                "comply_or_explain_case_handled_by": "",
                "comply_or_explain_explained_by": "",
                "comply_or_explain_explained_on": None,
                "comply_or_explain_explanation": "",
                "comply_or_explain_explanation_valid_until": None,
                "comply_or_explain_is_explained": False,
                "endpoint": ep.pk,
                "evidence": "test",
                "explanation": "0",
                "is_the_latest_scan": False,
                "last_scan_moment": "2025-03-02T00:00:00Z",
                "meaning": "{}",
                "prr_is_progress": False,
                "prr_is_regression": False,
                "prr_was_previously": 3,
                "rating": "info",
                "rating_determined_on": "2025-03-02T00:00:00Z",
                "type": "nuclei_exposed_panels",
            },
            "model": "scanners.endpointgenericscan",
            "pk": epgs.pk,
        },
    ]
