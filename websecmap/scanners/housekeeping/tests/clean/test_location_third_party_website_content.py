import json
from datetime import datetime, timezone

from websecmap.organizations.models import Url
from websecmap.scanners.models import (
    Endpoint,
    EndpointGenericScan,
)

from websecmap.scanners.scanner_for_everything.location import (
    SCAN_TYPE_LOCATION_THIRD_PARTY_WEBSITE_CONTENT,
)
from websecmap.scanners.housekeeping.clean.location_third_party_website_content import clean


def test_remove_prefix_from_found_locations(db):
    evidence_1 = [
        {
            "rating": "location_ok",
            "evidence": {
                "found_location": {
                    "continent_code": "EU",
                    "country_iso_code": "NL",
                    "city_in_en": "Amsterdam",
                    "as_number": 8075,
                    "as_organization": "MICROSOFT-CORP-MSN-AS-BLOCK",
                    "isp_ame": "Microsoft Azure",
                    "network": "20.105.128.0/17",
                },
                "country_rating": "unknown",
                "continent_rating": "ok",
            },
            "domain": "api.socialschools.eu",
        },
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                    "network": "74.125.128.0/20",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "fonts.gstatic.com",
        },
        {
            "rating": "location_unknown",
            "evidence": {
                "found_location": {
                    "continent_code": None,
                    "country_iso_code": None,
                    "city_in_en": "",
                    "as_number": 13335,
                    "as_organization": "CLOUDFLARENET",
                    "isp_ame": "Cloudflare",
                    "network": "104.26.0.0/20",
                },
                "country_rating": "unknown",
                "continent_rating": "unknown",
            },
            "domain": "stichtingscala-live-72c73d5363d14aa6a2-09160db.aldryn-media.com",
        },
    ]

    evidence_2 = [
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                    "network": "64.233.184.0/22",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "ajax.googleapis.com",
        },
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                    "network": "108.177.96.0/19",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "fonts.googleapis.com",
        },
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                    "network": "74.125.128.0/20",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "fonts.gstatic.com",
        },
    ]

    # satisfy not null constraints
    url = Url.objects.create(url="zutphen.nl")
    ep = Endpoint.objects.create(
        url=url, discovered_on=datetime.now(timezone.utc), port=443, protocol="https", ip_version=4
    )

    # latest scan and other fields are not relevant.
    epgs_1 = EndpointGenericScan.objects.create(
        endpoint=ep,
        rating="location_low",
        evidence=json.dumps(evidence_1),
        rating_determined_on=datetime.now(timezone.utc),
        type=SCAN_TYPE_LOCATION_THIRD_PARTY_WEBSITE_CONTENT,
    )

    epgs_2 = EndpointGenericScan.objects.create(
        endpoint=ep,
        rating="location_low",
        evidence=json.dumps(evidence_2),
        rating_determined_on=datetime.now(timezone.utc),
        type=SCAN_TYPE_LOCATION_THIRD_PARTY_WEBSITE_CONTENT,
    )

    clean(url)

    # the alphabetical order of domains stays the same...
    assert json.loads(EndpointGenericScan.objects.get(id=epgs_1.id).evidence) == [
        {
            "rating": "location_ok",
            "evidence": {
                "found_location": {
                    "continent_code": "EU",
                    "country_iso_code": "NL",
                    "city_in_en": "Amsterdam",
                    "as_number": 8075,
                    "as_organization": "MICROSOFT-CORP-MSN-AS-BLOCK",
                    "isp_ame": "Microsoft Azure",
                },
                "country_rating": "unknown",
                "continent_rating": "ok",
            },
            "domain": "api.socialschools.eu",
        },
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "fonts.gstatic.com",
        },
        {
            "rating": "location_unknown",
            "evidence": {
                "found_location": {
                    "continent_code": None,
                    "country_iso_code": None,
                    "city_in_en": "",
                    "as_number": 13335,
                    "as_organization": "CLOUDFLARENET",
                    "isp_ame": "Cloudflare",
                },
                "country_rating": "unknown",
                "continent_rating": "unknown",
            },
            "domain": "stichtingscala-live-72c73d5363d14aa6a2-09160db.aldryn-media.com",
        },
    ]

    assert json.loads(EndpointGenericScan.objects.get(id=epgs_2.id).evidence) == [
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "ajax.googleapis.com",
        },
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "fonts.googleapis.com",
        },
        {
            "rating": "location_low",
            "evidence": {
                "found_location": {
                    "continent_code": "NA",
                    "country_iso_code": "US",
                    "city_in_en": "",
                    "as_number": 15169,
                    "as_organization": "GOOGLE",
                    "isp_ame": "Google Servers",
                },
                "country_rating": "unknown",
                "continent_rating": "low",
            },
            "domain": "fonts.gstatic.com",
        },
    ]
