import logging
from datetime import timedelta

from websecmap.organizations.models import Url
from websecmap.scanners.models import (
    Endpoint,
    EndpointGenericScan,
    Screenshot,
    HttpEndpointHeaders,
    HttpEndpointContent,
)

log = logging.getLogger(__package__)


def deduplicate(url: Url, outage_days: int = 60):
    """
    This iterates over all endpoints from new to old and removes the newest stuff if the older endpoint exists.
    This assumes that you can have gaps between endpoint detection of N days, those gaps will be closed.
    These gaps can be caused by network outages (usually) or bugs.

    It will move stuff to the older endpoint and delete the newer endpoint. If the newer endpoint is alive, the
    older endpoint that was marked as dead will be set to alive.

    It's not the fastest algorithm, but it's easy to understand and maintain. That helps a lot :)

    Days: the amount of days between the current endpoint and the last endpoint. This is the maximum gap that will
    be merged.

    Duplicates can be created in case of dns outages. In that case endpoints are set to deleted and
    created afterward when the dns is restored. In that case, a manual action may lead to "restoring"
    of the database, but in reality a set of queued "new endpoints" will also be created: so both
    the system created endpoints and the administrator made alive endpoints.
    """

    # Iterate over all endpoints from new to old. So all endpoints exist. id descending, newest first.
    for endpoint_id in (
        Endpoint.objects.all().filter(url=url).order_by("-discovered_on").only("id").values_list("id", flat=True)
    ):
        log.info("Attempting to deduplicate: %s", endpoint_id)
        # Always fetch the latest endpoint state, because it might have been changed by an update.
        fresh_endpoint = Endpoint.objects.all().filter(id=endpoint_id).first()
        if not fresh_endpoint:
            continue
        reduce_if_duplicate_endpoint(fresh_endpoint, outage_days)
    log.info("done")


def reduce_if_duplicate_endpoint(endpoint: Endpoint, outage_days: int = 60):
    # Do just one pass: if there is an older endpoint, remove the current one and switch to the older one.
    # Copy the state of the current one to the older one so it's probably alive.
    older_endpoint = get_older_duplicate_endpoint(endpoint, within_days=outage_days)
    if not older_endpoint:
        log.info("No older endpoint exists for %s in the last %s days.", endpoint, outage_days)
        return

    log.info("Going to transfer data from %s to %s.", endpoint, older_endpoint)
    # Move all the older stuff to the older endpoint, but set the older endpoint to the current state.
    # This will make the endpoint alive, probably, as that is most likely to be the current state.
    # Afterward this endpoint becomes useless and can be deleted.
    # When transitioning all scans of this endpoint, there might be duplicates, where the older might
    # have explanations already.
    transfer_endpoint_data(from_endpoint=endpoint, to_endpoint=older_endpoint)
    transfer_endpoint_state(from_endpoint=endpoint, to_endpoint=older_endpoint)
    # deduplicate_scans(older_endpoint)
    delete_endpoint(endpoint)


def delete_endpoint(endpoint: Endpoint):
    endpoint.delete()


def transfer_endpoint_data(from_endpoint: Endpoint, to_endpoint: Endpoint):
    EndpointGenericScan.objects.all().filter(endpoint=from_endpoint).update(endpoint=to_endpoint)
    Screenshot.objects.all().filter(endpoint=from_endpoint).update(endpoint=to_endpoint)
    HttpEndpointHeaders.objects.all().filter(endpoint=from_endpoint).update(endpoint=to_endpoint)
    HttpEndpointContent.objects.all().filter(endpoint=from_endpoint).update(endpoint=to_endpoint)


def transfer_endpoint_state(from_endpoint: Endpoint, to_endpoint: Endpoint):
    to_endpoint.is_dead = from_endpoint.is_dead
    to_endpoint.is_dead_since = from_endpoint.is_dead_since
    to_endpoint.is_dead_reason = from_endpoint.is_dead_reason
    to_endpoint.save()


def get_older_duplicate_endpoint(endpoint: Endpoint, within_days: int = 60):
    # Determine if there is an older duplicate endpoint. If that exists, then return it.

    # No discovery date, doesn't make sense, can't deal with that.
    if endpoint.discovered_on is None:
        return None

    # created in a similar timefrime is nice, but also "deleted" a day before the current one is
    # created makes sense.
    # So let's check both:
    log.debug("Do we have a similar endpoint for %s created in the last %s days?", endpoint, within_days)
    by_creation_date = (
        Endpoint.objects.all()
        .filter(
            url=endpoint.url,
            protocol=endpoint.protocol,
            port=endpoint.port,
            ip_version=endpoint.ip_version,
            # can only be newer than N days
            discovered_on__gte=endpoint.discovered_on - timedelta(days=within_days),
            # has to be older, can't be newer than yourself.
            discovered_on__lte=endpoint.discovered_on,
        )
        .exclude(pk=endpoint.pk)
        # id descending, so the newest one first.
        .order_by("-discovered_on")
        .first()
    )
    if by_creation_date:
        return by_creation_date

    # there can be years between creation date, but between deletion and creation is very short
    log.debug(
        "Do we have a similar endpoint for %s that has been deleted within %s days before creation of this endpoint?",
        endpoint,
        within_days,
    )
    by_deletion_date = (
        Endpoint.objects.all()
        .filter(
            url=endpoint.url,
            protocol=endpoint.protocol,
            port=endpoint.port,
            ip_version=endpoint.ip_version,
            # can only be newer than N days
            is_dead_since__gte=endpoint.discovered_on - timedelta(days=within_days),
            # has to be older, can't be newer than yourself.
            is_dead_since__lte=endpoint.discovered_on,
        )
        .exclude(pk=endpoint.pk)
        # id descending, so the newest one first.
        .order_by("-is_dead_since")
        .first()
    )

    if by_deletion_date:
        return by_deletion_date

    return None
