# Django model serializer that creates a fixture to be used in loaddata. This can be used for tests.
# This allows a user to create a dumpdata for a specific record in a model with all relations.
# That kind of data comes in super handy for tests where extensive amounts of data is needed.
# https://docs.djangoproject.com/en/5.1/topics/serialization/

from django.core import serializers


from websecmap.organizations.models import (
    Url,
    Organization,
    OrganizationType,
    VerifiedDomainRegistrant,
    OrganizationSurrogateId,
    Coordinate,
    AlternativeName,
)
from websecmap.scanners.models import Endpoint, UrlGenericScan, EndpointGenericScan, HttpEndpointHeaders, Screenshot


def export_organization(organization: Organization) -> str:
    return serializers.serialize(
        "json",
        [
            # organization points to organizationType (as in 'is on layers'), so first we need OrganizationType
            *OrganizationType.objects.all().filter(organization_on_layer=organization),
            *AlternativeName.objects.all().filter(organization=organization),
            *Organization.objects.all().filter(pk=organization.pk),
            *Coordinate.objects.all().filter(organization=organization),
            *VerifiedDomainRegistrant.objects.all().filter(organization=organization),
            *OrganizationSurrogateId.objects.all().filter(organization=organization),
            # do not export organizationReport, even though there is a foreign key. As these reports can be rebuilt
            # using all metrics that are already in the export.
            # and everything related to urls / scans
            *Url.objects.all().filter(organization=organization),
            *Endpoint.objects.all().filter(url__organization=organization),
            *UrlGenericScan.objects.all().filter(url__organization=organization),
            *EndpointGenericScan.objects.all().filter(endpoint__url__organization=organization),
            *HttpEndpointHeaders.objects.all().filter(endpoint__url__organization=organization),
            *Screenshot.objects.all().filter(endpoint__url__organization=organization),
        ],
    )


def export_url(url: Url) -> str:
    # The extra asterisk is just copied from the manual.
    return serializers.serialize(
        "json",
        [
            *Url.objects.all().filter(pk=url.pk),
            *Endpoint.objects.all().filter(url=url),
            *UrlGenericScan.objects.all().filter(url=url),
            *EndpointGenericScan.objects.all().filter(endpoint__url=url),
            *HttpEndpointHeaders.objects.all().filter(endpoint__url=url),
            *Screenshot.objects.all().filter(endpoint__url=url),
        ],
    )


def export_superficial_layer(layer: OrganizationType) -> str:
    return serializers.serialize(
        "json",
        [
            # organizations will point to non-existing layers when exporting them completely.
            *OrganizationType.objects.all(),
            # same goes for alternative names shared between organizations
            *AlternativeName.objects.all(),
            *Organization.objects.all().filter(),
            # organizations usually do not share the same coordinate, so guess this will be fine...
            *Coordinate.objects.all().filter(organization__layers=layer),
            # will deliver urls that are also share with other organizations that are not in the export.
            # hence all organizations are exported. It's hard to remove the organizations from the result set.
            *Url.objects.all().filter(organization__layers=layer),
        ],
    )
