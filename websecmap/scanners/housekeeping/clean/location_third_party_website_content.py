import contextlib
import json
import logging
from json import JSONDecodeError

from websecmap.organizations.models import Url
from websecmap.scanners.models import EndpointGenericScan

from websecmap.scanners.scanner_for_everything.location import SCAN_TYPE_LOCATION_THIRD_PARTY_WEBSITE_CONTENT

log = logging.getLogger(__name__)


def clean(url: Url):
    # The network (aka prefix) on the AS changes frequently, every time a measurement is done.
    # A large AS means there are hundreds of prefixes. This changes every time while the AS stays the same.
    # To save millions of database records we're removing the "network" field from the location data.
    # This has also been removed from other code in the location scans.
    # examples:
    # as_number": 54113, "as_organization": "FASTLY", "isp_ame": "Fastly", "network": "151.101.64.0/22"
    # as_number": 54113, "as_organization": "FASTLY", "isp_ame": "Fastly", "network": "146.75.120.0/22
    # as_number": 54113, "as_organization": "FASTLY", "isp_ame": "Fastly", "network": "151.101.128.0/22
    # this is again done "per record" as that is the only way we can perform this while not timing out on queries
    # and not hinder any other database operations. See remove_random_data_from_existing_third_party_requests
    # todo: also perform this on url metric "SCAN_TYPE_LOCATION_SERVER" which caused millions of records.
    scans = EndpointGenericScan.objects.filter(
        endpoint__url=url, type=SCAN_TYPE_LOCATION_THIRD_PARTY_WEBSITE_CONTENT
    ).only("id")
    for scan in scans:
        print(f"Removing prefix from scan id {scan.id}")
        remove_prefix_from_found_location(scan.id)


def remove_prefix_from_found_location(scan_id: int):
    # todo: hoe zit dat met location in cookie metingen.
    # En kunnen we links naar het AS toevoegen in het rapport? Dan kan je zien van wie het is.
    scan_result = EndpointGenericScan.objects.filter(id=scan_id).first()
    if not scan_result:
        return

    # this prevents a "local variable 'evidence' referenced before assigment" error. Which, according to both
    # code inspection and linters could never occur. But it does, i've seen that happen. So here you go.
    evidence = {}
    with contextlib.suppress(JSONDecodeError):
        evidence = json.loads(scan_result.evidence)

    if not evidence:
        return

    for location in evidence:
        if location.get("evidence", {}).get("found_location", {}).get("network"):
            del location["evidence"]["found_location"]["network"]

    scan_result.evidence = json.dumps(evidence)
    scan_result.save(update_fields=["evidence"])
