from websecmap.organizations.models import Url
from websecmap.scanners.models import EndpointGenericScan
from websecmap.scanners.scanner.plain_http import clean_can_connect_result_evidence


def clean(url: Url):
    scans = EndpointGenericScan.objects.all().filter(
        endpoint__url=url, type="plain_http", rating="no_https_redirect_and_no_https_on_standard_port"
    )

    for scan in scans:
        if not scan.evidence:
            return

        scan.evidence = clean_can_connect_result_evidence(scan.evidence)
        scan.save(update_fields=["evidence"])
