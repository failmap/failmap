# todo: when removing this file, all tests still seem to run. Is this just a file that should be deleted?

import pytest
from django.test.runner import DiscoverRunner


class PytestTestRunner(DiscoverRunner):
    """Runs pytest to discover and run tests."""

    pdb = False

    def __init__(self, verbosity=1, failfast=False, keepdb=False, pdb=False, **kwargs):
        self.verbosity = verbosity
        self.failfast = failfast
        self.keepdb = keepdb
        self.pdb = pdb
        super().__init__()

    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument(
            "--pdb", action="store_true", default=False, dest="pdb", help="Drop into PDB on test failure."
        )
        DiscoverRunner.add_arguments(parser)

    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        """Run pytest and return the exitcode.

        It translates some of Django's test command option to pytest's.
        """

        argv = []
        if self.verbosity == 0:
            argv.append("--quiet")
        if self.verbosity == 2:
            argv.append("--verbose")
        if self.verbosity == 3:
            argv.append("-vv")
        if self.failfast:
            argv.append("--exitfirst")
        if self.keepdb:
            argv.append("--reuse-db")
        if self.pdb:
            argv.append("--pdb")

        argv.extend(test_labels)
        return pytest.main(argv)
