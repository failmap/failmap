import logging

from websecmap.organizations.datasources.utils import finish_import
from websecmap.organizations.models import GenericFileDataset, Organization, Url
from datetime import datetime, timezone
import json

log = logging.getLogger(__name__)

FILE_FORMAT = "rdapslurper_urls"


def import_rdapslurper_datasets():
    for dataset in GenericFileDataset.objects.all().filter(file_format=FILE_FORMAT, is_imported=False):
        import_rdapslurper_dataset(dataset)


def import_rdapslurper_dataset(dataset: GenericFileDataset):
    """
    File contents:
    [
      {
        "organisation_id": 18090,
        "url": "tradetalk.nl"
      },
    ]
    """
    log.debug("Importing dataset %s", dataset.id)
    log.debug("Opening file %s", dataset.file.path)
    with open(dataset.file.path, "r", encoding="UTF-8") as file:
        data = json.load(file)

    log.debug("Parsed %s records", len(data))

    orgs_cache = {
        # Cache organization IDs to avoid unnecessary database queries
    }
    added_urls = 0
    for record in data:
        org_id = record["organisation_id"]
        url_url = record["url"]
        # find the organization or retrieve it from the database.
        org = orgs_cache.get(org_id, Organization.objects.filter(id=org_id).first())
        orgs_cache[org_id] = org

        if not org:
            log.warning("Organization %s not found", org_id)
            continue

        # not placing existing urls into other organizations.
        if Url.objects.filter(url=url_url).exists():
            log.warning("Url %s already exists", url_url)
            continue

        added_urls += 1

        url = Url.objects.create(
            url=url_url, is_dead=False, created_on=datetime.now(timezone.utc), origin="whois_match_sidn"
        )
        url.organization.add(org)

    dataset.state = f"Imported {added_urls} urls."

    finish_import(dataset)
