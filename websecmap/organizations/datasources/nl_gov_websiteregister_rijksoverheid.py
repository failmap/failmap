import logging
import re
from datetime import datetime, timezone
from typing import Dict, List, Tuple

import requests
import tldextract
from django.core.files.uploadedfile import SimpleUploadedFile
from pyexcel_ods import get_data
from requests import RequestException

from websecmap.app.constance import constance_cached_value
from websecmap.celery import app
from websecmap.organizations.models import GenericFileDataset, Organization, OrganizationSurrogateId, Url

log = logging.getLogger(__name__)


def find_current_websiteregister_download_link() -> str:
    """
    Retrieve the current websiteregister ods file from a configurable location.
    This prevents the need to work with spreadsheets. OTOH working with spreadsheet uploads gives much more
    control but can't be automated properly. It probably will be available for download for years on the
    same page, but with a different filename. It's from 2016 onward the same location.

    I guess you want both, and the spreadsheet upload and the automated process.

    # https://www.communicatierijk.nl/vakkennis/r/rijkswebsites/verplichte-richtlijnen/websiteregister-rijksoverheid

    """
    location = constance_cached_value("WEBSITEREGISTER_DOWNLOAD_PAGE")
    pat = constance_cached_value("WEBSITEREGISTER_SPREADSHEET_FILE_PATTERN_ON_PAGE")

    domain = tldextract.extract(location)

    try:
        data = requests.get(location, timeout=300)
        match = re.findall(pat, data.text)
        if not match:
            return ""

        url = match.pop()
        if not url:
            return ""

        return f"https://{domain.fqdn}/{url}"

    except RequestException as exc:
        log.error("Could not download data from %s. Or pattern did not match: %s", location, pat)
        log.exception(exc)
        return ""


def download_websiteregister_spreadsheet() -> int:
    """
    Retrieve the current websiteregister ods file from a configurable location.
    This prevents the need to work with spreadsheets. OTOH working with spreadsheet uploads gives much more
    control but can't be automated properly. It probably will be available for download for years on the
    same page, but with a different filename. It's from 2016 onward the same location.

    I guess you want both, and the spreadsheet upload and the automated process.

    # https://www.communicatierijk.nl/vakkennis/r/rijkswebsites/verplichte-richtlijnen/websiteregister-rijksoverheid

    """

    idataset = GenericFileDataset()
    idataset.file_format = "websiteregister_rijksoverheid"
    idataset.is_imported = False

    now = datetime.now(timezone.utc).isoformat()

    download_link = find_current_websiteregister_download_link()
    if not download_link:
        idataset.origin = f"Failed to find download url on download page, on {now}."
        idataset.state = "failed_to_find_spreadsheet_download_url"
    else:
        try:
            response = requests.get(download_link, timeout=300)
            idataset.file = SimpleUploadedFile(f"websiteregister_rijksoverheid_{now}.ods", response.content)
            idataset.origin = (
                f"Automatic download from Websiteregister Rijksoverheid, on"
                f" {datetime.now(timezone.utc).isoformat()}."
            )
            idataset.state = "uploaded"
        except RequestException as exc:
            idataset.origin = f"Failed download from Websiteregister Rijksoverheid, on {now}. Exception: {str(exc)}."
            idataset.state = "failed_file_download"

    idataset.save()
    return idataset.id


@app.task(queue="database", ignore_result=True)
def download_and_import():
    downloaded_file = download_websiteregister_spreadsheet()
    import_file(downloaded_file)


def import_file(generic_file_dataset_id: int):
    download = GenericFileDataset.objects.all().get(id=generic_file_dataset_id)
    if not download.state == "uploaded":
        return

    error_domains, success_domains = import_websiteregister(download.file.path)
    state = f"imported {len(success_domains)} domains sucessfully and {len(error_domains)} failed. "
    if error_domains:
        state += f"Amongst failures are: {', '.join(error_domains)}"

    download.state = state[:3999]

    download.is_imported = True
    download.imported_on = datetime.now(timezone.utc)
    download.save()


def import_websiteregister(file_location: str) -> Tuple[List[str], List[str]]:
    rows = get_rows(file_location)
    results = [match_and_place_row(row) for row in rows]

    # give some nice UI feedback about what failed and what was successful
    error_domains = [result["domain"] for result in results if result["state"] == "error"]
    success_domains = [result["domain"] for result in results if result["state"] == "success"]

    return error_domains, success_domains


def get_rows(file_location: str) -> List[List[str]]:
    """
    res = {
        'Sheet1':
            [
                [1797],
                ['URL',
                 'Organisatietype',
                 'Organisatie',
                 'Suborganisatie',
                 'Afdeling'],
                ['http://www.rijksoverheid.nl',
                 'Rijksoverheid',
                 'AZ',
                 'DPC',
                 'Online Advies'],
                ['http://www.nederlandwereldwijd.nl', 'Rijksoverheid', 'BUZA'],
                ['http://coronadashboard.rijksoverheid.nl',
                 'Rijksoverheid',
                 'VWS',
                 '',
                 'Kerndepartement/PDC19']
            ]
        }
    """

    try:
        # in 2023 or 2024 another defining column for the name has been added...
        data = get_data(file_location, start_column=0, column_limit=6)
    except FileNotFoundError:
        log.error("Could not find spreadsheet for import on location: %s", file_location)
        return []
    # Plethora of possible exceptions, for malformed data, io issues, and whatnot.
    except BaseException as exc:  # noqa  # pylint: disable=broad-except
        log.error("Unexpected error importing spreadsheet on location: %s, error: %s", file_location, str(exc))
        return []

    # The first and second row do not contain domains, the other rows might be useful
    return [row for row in data["Sheet1"] if len(row) > 1 and str(row[0]).startswith("http")]


def match_and_place_row(row: List[str]) -> Dict[str, str]:
    # 1: Do NOT add domains that are already in the database, as they are already placed to the right owner
    # and re-importing them will place them under the wrong owner if the register is wrong.
    # The register sometimes contains mistakes which means we have to correct this over and over again.
    # 2: do NOT add the 2nd level domain if it has a non www. subdomain. Sudomains are placed and owned
    # by other organizations. And many times the organization just owns this only subdomain.
    # so: abc.rijksoverheid.nl should not also add rijksoverheid.nl to the organization.
    log.debug("Importing row: %s", row)

    # calculate the surrogate key of this organization
    # 2022: The key for th eorganiztaion name was: (DPC/AZ)
    # 2024, add a new key DPCWR, and only use an alternative name. The website register changes
    # the names and such all the time which prevents full imports over time.
    # This is now stored in the surrogate key field as the data in the name has little to do anymore
    # with being human-friendly. The key will also be made smaller.
    if len(row) > 4:
        organization_surrogate_key = f"{row[2].strip()} {row[3].strip()} {row[4].strip()}"
    elif len(row) > 3:
        organization_surrogate_key = f"{row[2].strip()} {row[3].strip()}"
    else:
        organization_surrogate_key = f"{row[2].strip()}"

    # So first rule 2: make sure that it's either the domain listed or the domain without www.
    domain_extract = tldextract.extract(row[0])
    if domain_extract.subdomain == "www":
        # the www domain, as well as any other subdomain will automatically be found via another scanner
        domain = f"{domain_extract.domain}.{domain_extract.suffix}"
    else:
        domain = domain_extract.fqdn

    # Now rule 1: make sure it's not in the database yet
    if Url.objects.filter(url=domain).first():
        return {"state": "already_present", "domain": domain, "key": organization_surrogate_key}

    # find the organization that use this surrogate
    organizations = get_organizations_by_surrogate_key(
        organization_surrogate_key, "websiteregister_rijksoverheid", "websiteregister_rijksoverheid"
    )

    if not organizations:
        log.error("Could not find an organization %s, could not place domains %s.", organization_surrogate_key, domain)
        return {"state": "error", "domain": domain, "key": organization_surrogate_key}

    for organization in organizations:
        organization.add_url(domain, origin="import dutch websiteregister")

    return {"state": "success", "domain": domain, "key": organization_surrogate_key}


def get_organizations_by_surrogate_key(
    key: str,
    surrogate_id_name: str = "websiteregister_rijksoverheid",
    used_in_dataset: str = "websiteregister_rijksoverheid",
) -> List[Organization]:
    surrogates = OrganizationSurrogateId.objects.all().filter(
        surrogate_id=key,
        surrogate_id_name=surrogate_id_name,
        used_in_dataset=used_in_dataset,
        organization__is_dead=False,
    )
    return list({surrogate.organization for surrogate in surrogates})
