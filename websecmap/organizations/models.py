import hashlib
import json
import logging
import re
from datetime import datetime, timezone, timedelta
from typing import List, Optional

import tldextract
from django.core.exceptions import ValidationError
from django.db import IntegrityError, models, transaction
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
from jsonfield import JSONField
from taggit.managers import TaggableManager
from validators import domain

log = logging.getLogger(__package__)


class OrganizationType(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("layer"))
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        verbose_name = _("layer")
        verbose_name_plural = _("layers")

    def __str__(self) -> str:
        return str(self.name)


def validate_twitter(value):
    if value[0:1] != "@":
        raise ValidationError("Twitter handle needs to start with an @ symbol.")


class AlternativeName(models.Model):
    name = models.CharField(max_length=250)
    description = models.CharField(max_length=250, blank=True, null=True)
    tags = TaggableManager(blank=True)

    def __str__(self):
        return f"(alt: {self.name})"


class Organization(models.Model):
    country = CountryField(db_index=True)

    layers = models.ManyToManyField(OrganizationType, related_name="organization_on_layer")

    name = models.CharField(max_length=250, db_index=True)

    # different data sources use different names for the same organization. Using a list of alternative names
    # makes it easy find the same organization between imports. Implementing this as a relation, it will also
    # help answer the question: who uses these names which can be convenient for debugging.
    alternative_names = models.ManyToManyField(AlternativeName, blank=True)

    written_address = models.CharField(
        max_length=250, help_text="The written address that can be used for geocoding.", blank=True
    )

    city = models.CharField(
        # https://en.wikipedia.org/wiki/List_of_long_place_names
        # but also keep space to allow annotations if that's needed for rare cases. So add burrough etc.
        # like: Amsterdam (Zuid-Oost). This field is not factually correct.
        max_length=150,
        blank=True,
        null=False,
        default="",
        help_text="The city is part of the natural key for humans to find the right organizations when multiple "
        "organizations have the same name. This happens with primary schools a lot. There are even "
        "multiple ones with the same name in the same city. If that really becomes a problem a burough "
        "or district field should be added for further specification. For rare cases this field can just"
        " be used to contain city and disctrict. Like: Amsterdam (Zuid-Oost).",
    )

    computed_name_slug = models.SlugField(
        max_length=250,
        help_text="Computed value, a slug translation of the organization name, which can be used in urls.",
        default="",
    )

    internal_notes = models.TextField(
        max_length=1500,
        help_text="These notes can contain information on WHY this organization was added. Can be handy if it's not "
        "straightforward. This helps with answering questions why the organization was added lateron. "
        "These notes will not be published, but are also not secret.",
        blank=True,
        null=True,
    )

    twitter_handle = models.CharField(
        max_length=150,
        help_text="Include the @ symbol. Used in the top lists to let visitors tweet to the"
        "organization to wake them up.",
        null=True,
        blank=True,
        validators=[validate_twitter],
    )

    # stacking is_dead pattern
    # postpone migration on production.
    created_on = models.DateTimeField(
        blank=True,
        null=True,
        default=datetime(year=2016, month=1, day=1, hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone.utc),
        db_index=True,
    )

    is_dead_since = models.DateTimeField(blank=True, null=True, db_index=True)

    is_dead = models.BooleanField(
        default=False, help_text="A dead organization is not shown on the map, depending on the dead_date."
    )

    is_dead_reason = models.CharField(max_length=255, blank=True, null=True)

    wikidata = models.CharField(
        max_length=255, blank=True, null=True, help_text="Reference to the wikidata project. Example:Q9928"
    )

    wikipedia = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="Reference to the wikipedia article, including the correct wiki. "
        "Example: nl:Heemstede (Noord-Holland)",
    )

    surrogate_id = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="Any ID used to identify this organization in an external system. Used in automated imports via "
        "the API. Otherwise leave this field empty.",
    )

    computed_domain_registrant_statistics = JSONField(
        help_text="Statistics generated from the UrlWhois data, auto updated", default={}, blank=True
    )
    acceptable_domain_registrants = JSONField(
        help_text="List of registrants that are the same as this organization, "
        "this is used to find domains that are not in control of this "
        "organization and thus make an issue out of that. If this "
        "field is empty, no check will be performed. Know that you "
        "will have to sometimes check if organizations have changed "
        "their name. List of strings of registrant names.",
        default=[],
        blank=True,
    )
    reasoning_for_registrant_decisions = models.TextField(
        default="",
        blank=True,
        help_text="DEPRECATED",
    )
    structured_reasoning_for_registrant_decisions = JSONField(
        help_text="List of decision. "
        "Reasoning why a registrant has been rejected or allowed. Accepted registrants usually are "
        "organization names that direcly connect to the domain. Rejected registrants can for example be "
        "names of employees, other companies, commercial companies that are not the organization themselves."
        "For example: 'texel.nl' belongs to 'Municipality of Texel' and not 'godaddy', 'herman acker' or ''. "
        "This field is purely for documentation and knowledge transfer."
        'Example: [{"registrant": "", "reason": "empty registrant"}]',
        blank=True,
        default=[{"registrant": "", "reason": "empty registrant"}],
    )
    arbitrary_kv_data = JSONField(
        help_text="Any other data that is extracted from the datasource, in key value pairs. It depends on "
        "the datasource imported. Every time of organization has their own unique codes.",
        blank=True,
        default={},
    )

    @property
    def not_allowed_registrant_names(self):
        return [reg["registrant"] for reg in self.structured_reasoning_for_registrant_decisions]

    class Meta:
        managed = True
        db_table = "organization"
        verbose_name = _("organization")
        verbose_name_plural = _("organizations")

    # todo: find a smarter way to get the organization type name, instead of a related query... cached enums?
    # this list resets per restart. So if you do complex changes in these layers / types...
    organization_name_cache = {}

    def __str__(self):
        if self.is_dead:
            return f"✝ {self.name}, {self.country} ({self.created_on.strftime('%b %Y')})"

        return f"{self.name}, {self.country} ({self.created_on.strftime('%b %Y')})"

    @property
    def names(self) -> List[str]:
        return [self.name] + list(self.alternative_names.all().values_list("name", flat=True))

    @property
    def altnames(self) -> List[str]:
        return list(self.alternative_names.all().values_list("name", flat=True))

    def add_url(self, url: str, origin: str = "", metadata: dict = None):
        # add url to database with validation etc:
        url = Url.add(url, origin=origin, metadata=metadata)

        # then add it to the organization
        url.organization.add(self)
        url.save()

        return url

    def save(self, *args, **kwarg):
        # handle computed values
        self.computed_name_slug = slugify(self.name)
        super().save(*args, **kwarg)


class OrganizationSurrogateId(models.Model):
    # When importing datasets there are usually more than one surrogate ID.
    # BRIN, HDL, AGB, foreign key
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    surrogate_id = models.CharField(max_length=255, blank=True, null=True)
    surrogate_id_name = models.CharField(
        max_length=40,
        blank=True,
        null=True,
        help_text="The name of the surrogate, e.g. id, BRIN, hdl_id, agb_codes, etc",
    )
    used_in_dataset = models.CharField(
        max_length=40,
        blank=True,
        null=True,
        help_text="The dataset that the surrogate comes from. This is relevant for context as the 'id' field in"
        "one dataset is different than the 'id' in another one.",
    )

    def __str__(self):
        return f"{self.surrogate_id_name}: {self.surrogate_id} for {self.organization.name} ({self.organization.id})"


GEOJSON_TYPES = (
    ("MultiPolygon", "MultiPolygon"),
    ("MultiLineString", "MultiLineString"),
    ("MultiPoint", "MultiPoint"),
    ("Polygon", "Polygon"),
    ("LineString", "LineString"),
    ("Point", "Point"),
)


class Coordinate(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    geojsontype = models.CharField(db_column="geoJsonType", max_length=20, blank=True, null=True, choices=GEOJSON_TYPES)

    # Note that points are stored in lng, lat format
    # https://gis.stackexchange.com/questions/54065/leaflet-geojson-coordinate-problem
    area = JSONField(
        max_length=14000,
        blank=True,
        help_text="GeoJson using the WGS84 (EPSG 4326) projection. Use simplified geometries to "
        "reduce the amount of data to transfer. Editing both this and the edit_area, this will take "
        "preference.",
    )

    # 9e107d9d372bb6826bd81d3542a419d6 (16 bytes, or a string of 32 characters)
    calculated_area_hash = models.CharField(
        max_length=32,
        blank=True,
        null=True,
        help_text="Automatically calculated hash of the area field using the MD5 algorithm. This is used to"
        " try and optimize grouping on area (which is a very long text field, which is slow). The "
        " hope is that this field will increase the speed of which grouping happens. If it doesn't, "
        " we could calculate an even simpler hash by using the date + organization name."
        " Note that if only one field is used for an organization (multipolygon, etc) "
        "this field is not required... We still developed it because we forgot what we made...",
    )

    edit_area = JSONField(
        max_length=14000,
        null=True,
        blank=True,
        help_text="The results of this field are saved in the area and geojsontype. It's possible to edit the area"
        " field directly, which overwrites this field. Changing both the manual option takes preference.",
    )

    # stacking pattern for coordinates.
    created_on = models.DateTimeField(
        blank=True,
        null=True,
        db_index=True,
        default=datetime(year=2016, month=1, day=1, hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone.utc),
    )
    creation_metadata = models.CharField(max_length=255, blank=True, null=True)
    is_dead = models.BooleanField(
        default=False,
        help_text="Dead url's will not be rendered on the map. Scanners can set this check "
        "automatically (which might change in the future)",
    )
    is_dead_since = models.DateTimeField(blank=True, null=True, db_index=True)
    is_dead_reason = models.CharField(max_length=255, blank=True, null=True)

    def kill(self, reason: str = "Killed manually"):
        self.is_dead = True
        self.is_dead_since = datetime.now(timezone.utc)
        self.is_dead_reason = reason[:255]
        self.save()

    def revive(self):
        self.is_dead = False
        self.is_dead_since = None
        self.is_dead_reason = None
        self.save()

    def save(self, *args, **kwarg):
        # handle computed values
        self.calculated_area_hash = hashlib.md5(str(self.area).encode("utf-8")).hexdigest()
        super().save(*args, **kwarg)

    class Meta:
        managed = True
        db_table = "coordinate"
        verbose_name = _("coordinate")
        verbose_name_plural = _("coordinates")


class Url(models.Model):
    # make id explicit for type inference
    id = models.AutoField(primary_key=True)

    organization = models.ManyToManyField(Organization, related_name="u_many_o_upgrade")

    url = models.CharField(
        max_length=255, help_text="The FQDN. Lowercase url name. For example: mydomain.tld or subdomain.domain.tld"
    )

    internal_notes = models.TextField(
        max_length=500,
        help_text="These notes can contain information on WHY this URL was added. Can be handy if it's not "
        "straightforward. This helps with answering questions why the URL was added lateron. For example: "
        "some urls are owned via a 100% shareholder construction by a state company / municipality "
        "while the company itself is private. These notes will not be published, but are also not secret.",
        blank=True,
        null=True,
    )

    created_on = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    not_resolvable = models.BooleanField(
        default=False,
        help_text="Url is not resolvable (anymore) and will not be picked up by scanners anymore."
        "When the url is not resolvable, ratings from the past will still be shown(?)#",
    )

    not_resolvable_since = models.DateTimeField(blank=True, null=True)

    not_resolvable_reason = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="A scanner might find this not resolvable, " "some details about that are placed here.",
    )

    is_dead = models.BooleanField(
        default=False,
        help_text="Dead url's will not be rendered on the map. Scanners can set this check "
        "automatically (which might change in the future)",
    )

    is_dead_since = models.DateTimeField(blank=True, null=True)

    is_dead_reason = models.CharField(max_length=255, blank=True, null=True)

    uses_dns_wildcard = models.BooleanField(
        default=False,
        help_text="When true, this domain uses a DNS wildcard and any subdomain will resolve to "
        "something on this host.",
    )

    do_not_find_subdomains = models.BooleanField(
        default=False,
        help_text="If you do not want to automatically find subdomains, check this. This might be useful when "
        "a very, very large number of subdomains will be added for an organization and you only want to "
        "monitor a few urls that are relevant.",
    )

    dns_supports_mx = models.BooleanField(
        default=False,
        help_text="If there is at least one MX record available, so we can perform mail generic mail scans. (for these"
        "scans we don't need to know what mail-ports and protocols/endpoints are available).",
    )

    onboarding_stage = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        help_text="Because of complexity of onboarding, not working with Celery properly, onboarding is done in "
        "multiple steps. The last completed step is saved in this value. Empty: nothing. endpoints: endpoints"
        " have been found. completed: onboarding is done, also onboarded flag is set.",
    )

    computed_subdomain = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="Automatically computed by tldextract on save. Data entered manually will be overwritten.",
        db_index=True,
    )

    computed_domain = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="Automatically computed by tldextract on save. Data entered manually will be overwritten.",
    )

    computed_suffix = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="Automatically computed by tldextract on save. Data entered manually will be overwritten.",
    )

    onboarding_stage_set_on = models.DateTimeField(
        blank=True, null=True, help_text="When the onboarding stage was hit. Helps with time-outs."
    )

    contains_website_redirect = models.BooleanField(
        help_text="Shorthand to exclude redirects from scanning. The actual redirect is measured elsewhere and stored "
        "as a scan result. This works for any redirect: dns, http, javascript, etc. Not doing this on "
        "endpoint yet as that is too fine granularity. The intent for a domain is usually one.",
        default=False,
    )

    onboarded = models.BooleanField(
        default=False,
        help_text="After adding a url, there is an onboarding process that runs a set of tests."
        "These tests are usually run very quickly to get a first glimpse of the url."
        "This test is run once.",
    )

    onboarded_on = models.DateTimeField(blank=True, null=True, help_text="The moment the onboard process finished.")

    origin = models.CharField(
        max_length=60,
        default="",
        blank=True,
        null=True,
        help_text="Information on what has added this url. For example: what scanner.",
    )

    subdomain_iregexes_to_ignore = JSONField(
        blank=True,
        help_text="List[str] of iregex filters that will prevent a certain subdomain to be "
        "added. Note that this is applies for adding subdomains, not complete new urls "
        "which might include subdomains (that may be done later). Example iregex: "
        "vps[0-9]{1,6} ignores vps1243 and vps5231. Use this feature in case the "
        "do-not-add-subdomains flag cannot be set on a certain subdomain as that would also "
        "prevent important domains of this organizations to be added. Hosters often dump "
        "vm's and vps subdomains next to their own infrastructure under the same second "
        "level domain. Such as vps123.example.nl next to management.example.nl. In that case "
        "we _do_ want to have the management subdomain but not the vps123 subdomain.",
        default=[],
    )

    class Meta:
        managed = True
        db_table = "url"

    def as_dict(self):
        return {
            "id": self.id,
            "url": self.url,
        }

    def __str__(self):
        if self.is_dead:
            return f"✝ {self.url}"

        return self.url

    def kill(self, reason: str = ""):
        self.is_dead = True
        self.is_dead_since = datetime.now(timezone.utc)
        self.is_dead_reason = reason[:255]
        self.save()

    def make_unresolvable(self, message, date):
        self.not_resolvable = True
        self.not_resolvable_reason = message
        self.not_resolvable_since = date
        self.save()

    def clean(self):
        if self.is_dead and (not self.is_dead_since or not self.is_dead_reason):
            raise ValidationError(_("When telling this is dead, also enter the date and reason for it."))

        if Url.objects.all().filter(url=self.url, is_dead=False, not_resolvable=False).exclude(pk=self.pk).exists():
            raise ValidationError(_("Url already exists, existing url is alive and resolvable."))

        # urls must be lowercase
        self.url = self.url.lower()

        # !!!! below validation was placed in the admin interface.
        # We're adding the URL before we know it's allowed. This due to the missing docs on how to clean
        # many to many relationships. The URL needs to have an ID when querying a many to many for it, otherwise
        # you'll get an exception.
        # If it already exists, the url will be deleted still.
        # https://code.djangoproject.com/ticket/12938 - NOPE, not going to happen...
        # so we use plain old SQL and then it works fine :)
        # And that also won't work because organization is empty. Which is a total bummer. You'd expect
        # this field to be here somehow, but it isn't.
        # a warning might be possible after the insert, but then you've got two urls already.
        # this is really a shortcoming of Django.

    def save(self, *args, **kwarg):
        # handle computed values

        result = tldextract.extract(self.url)
        log.debug(result)
        self.computed_subdomain = result.subdomain
        self.computed_domain = result.domain
        self.computed_suffix = result.suffix

        super().save(*args, **kwarg)

    def is_top_level(self):
        # count the number of dots. Should be one.
        # allows your own extension on a lan. there are thousands of extensions today.
        # so do the stupid thing: trust user input :)
        if self.url.count(".") == 1:
            return True
        return False

    def subdomain_should_be_ignored(self, subdomain: str) -> bool:
        if not self.subdomain_iregexes_to_ignore:
            return False

        for regex in list(self.subdomain_iregexes_to_ignore):
            if re.fullmatch(regex, subdomain):
                log.debug("Not adding subdomain due to regex: %s", regex)
                return True

        return False

    @transaction.atomic
    def add_subdomain(self, subdomain, origin: str = "") -> Optional["Url"]:
        # import here to prevent circular/cyclic imports, this module imports Url.
        # pylint sees this, but python does not it seems.
        from websecmap.scanners.scanner.http import resolves  # pylint: disable=import-outside-toplevel, cyclic-import

        if not subdomain:
            return None

        new_url = f"{subdomain}.{self.url}".lower()

        if not Url.subdomain_adding_is_allowed(new_url):
            log.debug("Not allowed to add a subdomain, an above domain prevents this: %s", new_url)
            return None

        if not Url.is_valid_url(new_url):
            log.debug("Subdomain not valid: %s", new_url)
            return None

        # todo: this will not work anymore as the ownership can be completely different depending
        #  on the sub-sub domains etc.
        if Url.objects.all().filter(url=new_url, organization__in=self.organization.all()).exists():
            log.debug("Subdomain already in the database: %s", new_url)
            return None

        # todo: also add this to add_url? -> that code should check if it's a subdomain and if the domain exists etc
        #  in that case it would fall back to this check. In the future...
        if self.subdomain_should_be_ignored(subdomain):
            return None

        if not resolves(new_url):
            log.debug("New subdomain did not resolve on either ipv4 and ipv6: %s", new_url)
            return None

        # figure out ownership of this domain on the sub sub domain level. There can be many owners and
        # the owner of the current loaded domain might no be the owner of the subdomain.
        # see test_add_sub_sub_domain for examples.
        # A Url needs to have a value for field "id" before a many-to-many relationship can be used.
        owners = Url.find_domain_owner(new_url)
        if not owners:
            log.warning("Domain has no owner: %s", new_url)
            # let's still add the domain, but it will also not have owners.
            # return None

        # domain will now be added, but we need the ownership already otherwise we will check the
        # ownership of the current domain.
        try:
            with transaction.atomic():
                # we found something that gives the idea that transactions are not working.
                my_url, created = Url.objects.get_or_create(url=new_url)
                if not created:
                    log.warning(
                        "The url already existed in the database, even while all prior checks in "
                        "this transaction told us otherwise."
                    )
                    return None
        except IntegrityError as my_exception:
            log.error(my_exception)
            return None

        if origin:
            log.debug("Saving new URL: %s", new_url)
            my_url.origin = origin
            my_url.save()

        for organization in owners:
            my_url.organization.add(organization)
            my_url.save()
            log.info("Added url: %s to organization: %s", new_url, organization)

        return my_url

    @staticmethod
    def subdomain_adding_is_allowed(input_domain: str) -> bool:
        """
        Will split a domain by . and will try to find the current organizations that own the domain.
        If the domain does not exist yet, it will try again for a higher level up until the second level.

        a.b.c.domain.nl will try: a.b.c.domain.nl, then b.c.domain.nl, then c.domain.nl, then domain.nl
        """
        splitted = input_domain.split(".")

        # not possible
        if len(splitted) < 2:
            return False

        for i in range(len(splitted)):
            # dont do anything with top level domains.
            if len(splitted[i:]) == 1:
                continue

            current = ".".join(splitted[i:])
            log.debug("checking if adding is allowed on parent domain: %s", current)
            if Url.objects.all().filter(url=current).exists():
                # it's possible to not have an organization, which might be by design.
                # regular domains will have an organization.
                if Url.objects.all().filter(url=current).first().do_not_find_subdomains:
                    return False

        return True

    @staticmethod
    def find_domain_owner(input_domain: str) -> List[Organization]:
        """
        Will split a domain by . and will try to find the current organizations that own the domain.
        If the domain does not exist yet, it will try again for a higher level up until the second level.

        a.b.c.domain.nl will try: a.b.c.domain.nl, then b.c.domain.nl, then c.domain.nl, then domain.nl
        """
        splitted = input_domain.split(".")

        # not possible
        if len(splitted) < 2:
            log.debug("Domain is too short to be treated as such: %s. Not able to find owner.", input_domain)
            return []

        for i in range(len(splitted)):
            # dont do anything with top level domains.
            if len(splitted[i:]) == 1:
                log.debug("splitted is too short: %s. Not able to find owner.", splitted[i:])
                continue

            current = ".".join(splitted[i:])
            log.debug("testing domain: %s", current)
            # todo: perhaps just find something above...
            if Url.objects.all().filter(url=current).exists():
                # it's possible to not have an organization, which might be by design.
                # regular domains will have an organization.
                log.debug("Returning owner for domain: %s", current)
                owners = Url.objects.all().filter(url=current).first().organization.all()
                log.debug("Owners: %s", list(owners))
                return list(owners)

        log.debug("Could not find owner for domain: %s", input_domain)
        return []

    @staticmethod
    def is_valid_url(url: str) -> bool:
        # empty strings, etc
        if not url:
            log.debug("Domain is empty, so not valid.")
            return False

        extract = tldextract.extract(url)
        if not extract.suffix:
            log.debug("Domain %s has no suffix, so not valid.", url)
            return False

        # Validators catches 'most' invalid urls, but there are some issues and exceptions that are not really likely
        # to cause any major issues in our software. The other alternative is another library with other quircks.
        # see: https://github.com/kvesteri/validators/
        # Note that this library does not account for 'idna' / punycode encoded domains, so you have to convert
        # them yourself. luckily:
        # 'аренда.орг' -> 'xn--80aald4bq.xn--c1avg'
        # 'google.com' -> 'google.com'
        try:
            decoded = url.encode("idna").decode()
        except UnicodeError:
            log.debug("Could not decode url %s...", url)
            return False

        # validators.domain has an error in it's validation: it does not support subdomains that start with a underscore
        # yet this is perfectly valid. And given that _bla._.bla._bla.bla.com is hard to filter just accept underscores
        # for now everywhere, even if the underscore is not valid as the first character of a hostname.
        # work around that issue by not accepting that specific issue.
        if extract.domain.startswith("_"):
            log.debug("Domain %s starts with an underscore, so not valid.", url)
            return False

        decoded = decoded.replace("_", "")
        valid_domain = domain(decoded)
        if valid_domain is not True:
            # encoding with 'idna' codec failed (UnicodeError: label empty or too long)
            # .apple.com for example: this label is incorrect as it starts with a dot. You should sanitize this
            # beforehand.
            log.debug("Not %s valid according to validators.domain.", url)
            return False
        return True

    @staticmethod
    def add(url: str, origin: str = "", metadata: dict = None) -> Optional["Url"]:
        # origin: where the url came from, for example the name of the scanner.

        # Rebuild the url based on the information present:
        # Preventing idna issues, preventing ports, directories, usernames, passwords and other stuff in urls.
        extract = tldextract.extract(url)
        url = extract.fqdn

        if not Url.is_valid_url(url):
            raise ValueError(
                "Url is not valid. It does not follow idna spec or does not have a valid suffix. "
                "IP Addresses are not valid at this moment."
            )

        # it does not matter if the url is dead or not: if it is in the database, do not add it again.
        # We don't do url lifecycles, they are either alive or dead. Who owns them when is a different story.
        # when we set an url as dead, it will stay in the database in order to make sure it is not added via
        # another datasource again.
        existing_url = Url.objects.all().filter(url=url).first()
        if not existing_url:
            new_url = Url(url=url)
            new_url.created_on = datetime.now(timezone.utc)
            new_url.origin = origin
            try:
                new_url.internal_notes = json.dumps(metadata) if metadata else ""
            except json.JSONDecodeError:
                log.error("Failed to dump metadata as json: %s", metadata)
            new_url.save()
            return new_url

        return existing_url


class UrlWhois(models.Model):
    # contains whois records / info on domains, this helps with removing dropcatched domains.
    # in the past we've seen domains that have been abandoned but where still in the official domain metadata lists
    # of the government: thus a domain squatter used it to run ads and add tons of 'interesting' subdomains.
    url = models.ForeignKey(Url, on_delete=models.CASCADE)
    # create a new record when registration, creation_date, update_date
    at_when = models.DateTimeField(blank=True, null=True, help_text="", auto_now_add=True)
    updated_date = models.DateField(blank=True, null=True, help_text="")
    is_the_latest = models.BooleanField(default=False, help_text="Is this the latest record for this url?")

    # patterns are: registrar disappears, foreign address is used in reseller, new creationdate
    # these fields are added here to make it easy to view in the admin interface
    registrant = models.CharField(max_length=255, blank=True, null=True)
    creation_date = models.DateField(blank=True, null=True, help_text="")
    admin_contact = models.CharField(max_length=255, blank=True, null=True)
    reseller = models.CharField(max_length=255, blank=True, null=True)
    abuse = models.CharField(max_length=255, blank=True, null=True)

    whois_record_data = models.TextField()
    whois_record_type = models.CharField(max_length=10, blank=True, null=True, default="json")

    def __str__(self):
        return f"{self.updated_date} - {self.creation_date}"


def seven_days_in_the_future():
    return datetime.now(timezone.utc) + timedelta(days=7)


def today():
    return datetime.now(timezone.utc).today()


class Dataset(models.Model):
    """
    Allows you to define URL datasources to download and import into the system. This acts as a memory of what you
    have imported. You can even re-import the things listed here. It will use the generic/excel importer.
    """

    url_source = models.URLField(
        null=True,
        blank=True,
        help_text="Fill out either the URL or File source. - A url source hosts the data you wish to process, this "
        "can be an excel file. You can also upload the excel file below. This works great with online data "
        "sources that are published regularly. Make sure the parser exists as you cannot process any "
        "arbritrary download.",
    )

    file_source = models.FileField(
        null=True,
        blank=True,
        help_text="Fill out either the URL or File source. - "
        "A file upload has to be in a specific Excel format. You can download this format here: "
        "<a href='/static/websecmap/empty_organizations_import_file.xlsx'>empty file</a>. "
        "You can also download "
        "an example that shows how to enter the data correctly. You can download the example here: "
        "<a href='/static/websecmap/example_organizations_import_file.xlsx'>example file</a>. Any additional fields "
        "will be used and stored in the arbitrary_kv_data column. This can be useful for later processing.",
    )

    is_imported = models.BooleanField(
        default=False,
    )
    imported_on = models.DateTimeField(blank=True, null=True)
    type = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        choices=[("excel", "Excel"), ("dutch_government", "Dutch Government")],
        help_text="To determine what importer is needed.",
        default="excel",
    )

    kwargs = models.TextField(
        max_length=5000,
        blank=True,
        null=True,
        help_text="A JSON / dictionary with extra options for the parser to handle the dataset. "
        "This is different per parser. This field is highly coupled with the code of the parser.",
        default="{}",
    )


class GenericFileDataset(models.Model):
    # Process: a periodic downloader adds files over time. They are then parsed and added to the database.

    # Manual (and automatic) importer for the dutch Websiteregister Rijksoverheid source.
    file = models.FileField(
        null=True,
        blank=True,
        help_text="Add a file in the format of the websiteregister rijksoverheid.",
    )
    file_format = models.CharField(
        blank=True, null=True, help_text="Basically what parser should handle this file.", max_length=140
    )

    is_imported = models.BooleanField(
        default=False,
    )
    imported_on = models.DateTimeField(blank=True, null=True)
    state = models.CharField(blank=True, null=True, max_length=4000)
    parser_info = models.CharField(blank=True, null=True, max_length=4000)
    origin = models.CharField(
        blank=True,
        null=True,
        help_text="Some idea what downloaded the file in case of" "an automatic download and parsing.",
        max_length=250,
    )


class VerifiedDomainRegistrant(models.Model):
    """
    This is a manually verified registrant that is valid for a certain organization. This registrant will be used to
    automatically add domains to this organizations. Make sure this registrant is (fairly) unique.

    Do NOT add "Rijksoverheid" as a registrant, as this will cover ANY domain of the Rijksoverheid, regardless of
    what ministry or department it belongs to. Those domains need a special treatment.

    Same goes for Takeaway.com and Thuisbezorgd. They have thousands of domains.

    todo: on add, check if the name is not in the BlockedDomainRegistrant list.
    """

    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    registrant_name = models.CharField(max_length=255, help_text="Registrant known at SIDN / countries registrar")
    admin_email_address = models.CharField(
        max_length=255,
        default="",
        help_text="Some organizations use the same "
        "registrant name but use the e-mail to create "
        "a separation between customers and themselves.",
    )

    notes = models.TextField(blank=True)
    is_still_valid = models.BooleanField(
        default=True,
        help_text="Can be disabled over time, which helps to keep track "
        "of what was added when. This is for debugging etc.",
    )
    domains_added = models.ManyToManyField(
        Url,
        help_text="To understand where domains come from and to roll back in case of accidents.",
        through="DomainsAddedViaVerifiedRegistrant",
    )

    def __str__(self):
        return f"{self.organization} - {self.registrant_name}"

    def clean(self, *args, **kwargs):

        if self.registrant_name.lower() in [bdr.registrant_name for bdr in BlockedDomainRegistrant.objects.all()]:
            raise ValidationError(f"Registrant {self.registrant_name} is blocked and cannot be used.")


class DomainsAddedViaVerifiedRegistrant(models.Model):
    url = models.ForeignKey(Url, on_delete=models.CASCADE)
    registrant = models.ForeignKey(VerifiedDomainRegistrant, on_delete=models.CASCADE)
    at_when = models.DateTimeField(auto_now_add=True)


class BlockedDomainRegistrant(models.Model):
    """
    These registrants are not allowed to be owners of a domain in our system. They will be rejected for all new domains
    that are added to the database.

    Take care that Versio B.V. also registers their own corporate domains AND domains of their clients under the same
    name. This makes it very hard to block it.

    This is about NEW domains. So better to have Takeaway.com and such listed here, to prevent thousands of domains
    being added for other businesses and organizations. They can be seen as a domain registrar or reseller etc.

    Rijksoverheid included.
    """

    registrant_name = models.CharField(
        max_length=255, help_text="Registrant known at SIDN / countries registrar", blank=True
    )
    reason = models.CharField(
        max_length=40,
        help_text="Why this registrant is blocked, eg: anonymous, domain seller, too generic name, empty, etc",
    )

    def __str__(self):
        return f"{self.id} {self.registrant_name}"

    def save(self, *args, **kwargs):
        self.registrant_name = self.registrant_name.lower()
        return super().save(*args, **kwargs)


class DomainZoneDump(models.Model):
    """
    A dump of the .nl Zone with all domains and registrants. This can be used to sift through and connect new official
    verified domain registrants. This will also be used for auto-importing once.
    """

    domain = models.CharField(max_length=255)
    registrant = models.CharField(max_length=255, blank=True, null=True)
