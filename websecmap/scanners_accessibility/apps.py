from django.apps import AppConfig


class ScannerAccessibilityConfig(AppConfig):
    name = "websecmap.scanners_accessibility"

    def ready(self) -> None:
        # prevent circular import
        # pylint: disable=import-outside-toplevel
        from . import tasks

        self.tasks = tasks
