"""
Determine accessibility violations for websites.
"""

import json
from django import db
from pydantic import TypeAdapter, ValidationError
from django.urls import reverse
from sentry_sdk import last_event_id

from django.core.files.base import ContentFile

import logging
from typing_extensions import TypedDict


from celery import Signature


import niquests as requests


from websecmap.scanners_accessibility.models import ScanSession

from websecmap.celery import app
from websecmap.scanners import ObjectFilter


from websecmap.scanners.scanner.__init__ import url_filters, q_configurations_to_scan

from typing import Iterable
from websecmap.scanners.models import Url
from .models import Violations
import os
from django.conf import settings

from websecmap.app.constance import constance_cached_str_value

from typing import Any

log = logging.getLogger(__name__)


ACCESSIBILITY_API = os.environ.get("WSM_ACCESSIBILITY_API", "http://browser.tools.internal/accessibility")


def filter_urls(
    organizations_filter: ObjectFilter,
    urls_filter: ObjectFilter,
    endpoints_filter: ObjectFilter,
) -> Iterable[Url]:
    """Filter eligible urls based on provided filters."""

    urls = Url.objects.filter(
        # only active/configured/enabled organizationtypes/layers and/or countries
        q_configurations_to_scan(level="url"),
        # remove unresolvable or dead entries
        is_dead=False,
        not_resolvable=False,
        endpoint__is_dead=False,
        # ignore redirects
        contains_website_redirect=False,
        # only https websites
        endpoint__protocol__in=["https"],
        # on expected port
        endpoint__port__in=[443],
    )

    # apply user provided filters
    urls = url_filters(urls, organizations_filter, urls_filter, endpoints_filter)

    return urls.iterator()


def compose_manual_scan_task(
    organizations_filter: ObjectFilter,
    urls_filter: ObjectFilter,
    endpoints_filter: ObjectFilter,
    force=False,
) -> Iterable[Signature[None]]:
    """Interpret filters passed down and return list of tasks to be executed."""

    urls = filter_urls(organizations_filter, urls_filter, endpoints_filter)

    tasks = compose_scan_task(urls)

    return tasks


def compose_scan_task(urls: Iterable[Url]) -> Iterable[Signature[None]]:
    """Generate url based scan and store tasks for all input urls."""

    tasks = ((scan.si(ACCESSIBILITY_API, url.url) | store.s(url.id)) for url in urls)

    return tasks


class ResultMeta(TypedDict):
    session_id: int
    url: str
    stats: dict[str, (str | int)]
    stop_reason: str
    paper_trails: dict[str, str]


class AccessibilityResult(TypedDict):
    violations: list[dict[str, Any]]

    meta: ResultMeta


@app.task(queue="accessibility")
def scan(accessibility_service_url: str, url: str) -> AccessibilityResult | None:
    # just except the loudest way possible in case of exceptions.
    log.debug("Scanning accessibility on %s", url)

    # amount of time the crawl process may take
    timeout = 300
    # extra timeout to add for API/server stuff
    api_timeout = 300

    params = {
        "site": f"https://{url}",
        "timeout": timeout,
    }
    request = f"{accessibility_service_url}"

    try:
        log.debug(
            "Doing the equivalent of: curl --fail --max-time %s '%s?site=https://%s&timeout=%s'",
            timeout + api_timeout,
            request,
            url,
            timeout,
        )

        response = requests.get(request, params=params, timeout=timeout + api_timeout)
    except requests.exceptions.ReadTimeout:
        log.exception("Request timed out")
        return None

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError:
        log.exception("Request to tool failed")
        return None

    try:
        response_json = response.json()
    except requests.exceptions.InvalidJSONError:
        log.exception("Failed to parse response JSON")
        return None

    log.debug("Accessibility report returned %s", response_json)
    return response_json


class AccessibilityStorageResult(TypedDict, total=False):
    success: bool
    admin_url: str | None
    sentry_event_url: str | None


AccessibilityResultValidator = TypeAdapter(AccessibilityResult)


def store_task_failure(sentry_event_id: str | None) -> AccessibilityStorageResult:
    if sentry_event_id:
        return {
            "success": False,
            "sentry_event_url": f"https://{settings.SENTRY_PROJECT}.sentry.io/issues/?query={last_event_id()}",
        }
    return {"success": False}


# automatically retry task if database is unavailable
@app.task(queue="storage", autoretry_for=(db.OperationalError,), retry_kwargs={"countdown": 300})
def store(result: AccessibilityResult, url_id: int) -> AccessibilityStorageResult:
    try:
        # make sure Url object exists
        url = Url.objects.get(id=url_id)
    except Url.DoesNotExist:
        log.exception("Failed to get Url object, not saving result")
        return store_task_failure(last_event_id())

    if not result:
        log.error("got empty result from previous task")
        return store_task_failure(last_event_id())

    try:
        result = AccessibilityResultValidator.validate_python(result)
    except ValidationError:
        log.exception("invalid result from previous task")
        return store_task_failure(last_event_id())

    session_id = result["meta"]["session_id"]

    ar = ScanSession(url=url)
    ar.session_id = session_id
    ar.stop_reason = result["meta"]["stop_reason"]
    ar.violation_count = int(result["meta"]["stats"]["total_violations"])
    ar.stats = result["meta"]["stats"]
    ar.save()

    try:
        filename = f"{session_id}-{url.url}.json"
        name = f"accessibility/{session_id}/{filename}"
        # TODO: during development save pretty formatted for convenience, should be compressed eventually
        # the client can unpack all this for the (admin) user
        content = json.dumps(result["violations"], indent=2)
        v = Violations(filename=filename, file_object=ContentFile(content, name=name))
        v.save()
        ar.violations.add(v)
        ar.save()
    except BaseException:
        log.exception("Failed to save violations to file")
        return store_task_failure(last_event_id())

    return {
        "success": True,
        "admin_url": constance_cached_str_value("PROJECT_WEBSITE").rstrip("/")
        + "/"
        + reverse(f"admin:{ar._meta.app_label}_{ar._meta.model_name}_change", args=[ar.id]),
    }
