"""Integration tests of scanner commands."""

import pytest
import json
from pathlib import Path
from django.core.management import call_command
from websecmap.scanners_accessibility.models import ScanSession, Violations
from websecmap.scanners_accessibility.tasks import ACCESSIBILITY_API


@pytest.mark.django_db
def test_scan_accessibility(responses, faaloniae, default_scan_metadata):
    """Test screenshot scan task invocation from command line. This almost covers all code for this task."""

    example_data = (Path(__file__).parent / "test_example.json").open("r").read()
    responses.add(responses.GET, ACCESSIBILITY_API, body=example_data, headers={"Content-Type": "application/json"})

    result = json.loads(call_command("scan", "accessibility", "-v3", "-u", faaloniae["url"], "-m", "direct"))

    assert result, "no scan was performed, or filters resulted in empty queryset"
    assert result[0]["success"] is True, "scan and store did not complete successfully"

    assert len(ScanSession.objects.all()) == 1, "no scan session object was saved"
    assert ScanSession.objects.first().url == faaloniae["url"], "scansession data does not match"

    assert len(Violations.objects.all()) == 1, "no violations file object was saved"
    assert (
        Violations.objects.first().violations == json.loads(example_data)["violations"]
    ), "violations JSON could not be read from violations file object"
