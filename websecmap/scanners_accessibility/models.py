from django.db import models
from jsonfield import JSONField
from django.core.files.storage import storages
from websecmap.organizations.models import Url
from websecmap.scanners.models import PaperTrail
import json


class Violations(models.Model):
    filename = models.CharField(max_length=200, blank=True, null=True, help_text="Filename including extension")
    file_object = models.FileField(storage=storages["violations"], upload_to="violations/%Y/%m-%d/")

    @property
    def violations(self):
        return json.loads(self.file_object.read())


class ScanSession(models.Model):
    """Stores or references information regarding a specific accessibility scan."""

    # make id explicit for type inference
    id = models.AutoField(primary_key=True)
    at_when = models.DateTimeField(auto_now_add=True)

    url = models.ForeignKey(Url, on_delete=models.CASCADE, help_text="Url that was scanned")

    session_id = models.BigIntegerField(
        blank=True, null=True, help_text="Unique ID for the browser session the cookie was found"
    )

    stop_reason = models.CharField(
        max_length=200, blank=True, null=True, help_text="Reason why the scansession stopped"
    )

    violation_count = models.IntegerField(help_text="Total amount of violations found in this sessions")

    stats = JSONField(blank=True, help_text="KV pairs with scan session statistics", default={})

    violations = models.ManyToManyField(
        Violations,
        blank=True,
        help_text="Violations in Axe JSON format",
    )

    paper_trails = models.ManyToManyField(
        PaperTrail,
        blank=True,
        help_text="References to logs and information collected during the scan, useful for debugging.",
    )

    def __str__(self):
        return f"{self.url} - {self.session_id} ({self.violation_count})"
