from websecmap.reporting import diskreport

# register tasks
# str() because: Invalid object 'clean_unlinked_reports' in __all__, must contain only strings (invalid-all-object)
# Nope, because: Undefined variable name '' in __all__ (undefined-all-variable)
# cannot explicitly add diskreport.clean_unlinked_reports to __all__ because of unclear reasons.
# This means that diskreport.clean_unlinked_reports is seen as dead code, which is BAD.
__all__ = [diskreport]
