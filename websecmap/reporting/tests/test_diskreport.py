import os
from datetime import datetime, timezone

from websecmap.organizations.models import Organization
from websecmap.map.models import OrganizationReport
from websecmap.reporting.diskreport import (
    _read_json_from_gzip,
    location_on_disk,
    retrieve_report,
    sync_reports_from_db_to_disk,
    clean_unlinked_reports,
    store_report,
)
from websecmap.scanners.tests.test_plannedscan import create_organization


def test_report_syncing(db):
    stored_data = {"yolo": "swag"}

    # setup creates a report for an organization
    organization = create_organization()
    organizationreport = OrganizationReport()
    organizationreport.calculation = stored_data
    organizationreport.organization = organization
    organizationreport.at_when = datetime.now(timezone.utc)
    organizationreport.save()

    # saves the report to disk and removes calculations from the database
    sync_reports_from_db_to_disk()

    # the file was created, the report is retrievable
    first_report = OrganizationReport.objects.get(id=organizationreport.id)
    assert first_report.calculation is None
    assert os.path.isfile(location_on_disk(first_report.id, "OrganizationReport"))
    assert retrieve_report(first_report.id, "OrganizationReport") == stored_data
    assert _read_json_from_gzip(location_on_disk(first_report.id, "OrganizationReport")) == stored_data


def test_clean_unlinked_reports_empty(tmpdir, db, caplog):
    # running this will cause no deletions, as there are no reports in the database at all.
    # Whatever is existing on disk is by accident or made by a developer...
    clean_unlinked_reports(OrganizationReport)
    assert "No reports found in database or storage location" in caplog.text


def test_clean_unlinked_reports(tmpdir, db, caplog):
    # create 3 reports in the database
    org = Organization.objects.create(id=1337001, name="org1")

    OrganizationReport.objects.create(id=1337005, organization=org, at_when=datetime.now(timezone.utc))
    OrganizationReport.objects.create(id=1337006, organization=org, at_when=datetime.now(timezone.utc))
    OrganizationReport.objects.create(id=1337007, organization=org, at_when=datetime.now(timezone.utc))

    # other tests might have created organization reports, purge them so this test executes cleanly
    clean_unlinked_reports(OrganizationReport)

    # create 4 reports on disk, this means one of them will be deleted
    store_report(1337005, OrganizationReport, {"yolo": "swag"})
    store_report(1337006, OrganizationReport, {"yolo": "swag"})
    store_report(1337007, OrganizationReport, {"yolo": "swag"})
    store_report(1337008, OrganizationReport, {"yolo": "swag"})
    assert os.path.isfile(location_on_disk(1337005, OrganizationReport))

    cleaned_up = clean_unlinked_reports(OrganizationReport)
    assert "Cleaned up 1 unlinked reports." in caplog.text
    assert cleaned_up == 1

    # create some random unlinked reports
    for i in range(10):
        store_report(i, OrganizationReport, {"yolo": "swag"})

    cleaned_up = clean_unlinked_reports(OrganizationReport)
    assert cleaned_up == 10
